package com.demo.common.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import com.demo.utils.Global;
/**
 * web容器启动销毁监听器
 * @author Administrator
 *
 */
public class WebContextListener implements ServletContextListener {
	

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("========================容器停止==========================");
	}

	public void contextInitialized(ServletContextEvent arg0) {
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n======================================================================\r\n");
		sb.append("\r\n    欢迎使用 1"+Global.getConfig("productName")+"  - Powered By shuangerduo");
		sb.append("\r\n======================================================================\r\n");
		System.out.println(sb.toString());
		
	}
}
