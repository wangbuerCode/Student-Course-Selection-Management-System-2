package com.demo.service;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Notice;
import com.demo.dao.NoticeDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 系统公告Service
 */
@Service
@Transactional(readOnly = true)
public class NoticeService extends CrudService<NoticeDao, Notice> {

	public Notice get(String id) {
		return super.get(id);
	}
	
	public List<Notice> findList(Notice notice) {
		return super.findList(notice);
	}
	
	public PageInfo<Notice> findPage(Notice notice) {
		PageHelper.startPage(notice.getPageNo(), notice.getPageSize());
		List<Notice> list = super.findList(notice);
		PageInfo<Notice> pageInfo = new PageInfo<Notice>(list, notice.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(Notice notice) {
		if (StringUtils.isEmpty(notice.getId())) {
			notice.setId(UUID.randomUUID().toString());
			dao.insert(notice);
		}else{
			dao.update(notice);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Notice notice) {
		super.delete(notice);
	}
	
}