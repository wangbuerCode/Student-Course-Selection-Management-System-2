package com.demo.service;
import java.util.List;
import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Choice;
import com.demo.dao.ChoiceDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 *Service
 */
@Service
@Transactional(readOnly = true)
public class ChoiceService extends CrudService<ChoiceDao, Choice> {

	@Autowired
	private ChoiceDao choicedao;
	
	public Choice get(String id) {
		return super.get(id);
	}
	
	public List<Choice> findList(Choice choice) {
		return super.findList(choice);
	}
	
	public PageInfo<Choice> findPage(Choice choice) {
		PageHelper.startPage(choice.getPageNo(), choice.getPageSize());
		List<Choice> list = super.findList(choice);
		PageInfo<Choice> pageInfo = new PageInfo<Choice>(list, choice.getPageSize());
		return pageInfo;
	}
	
	public PageInfo<Choice> findPages(Choice choice) {
		PageHelper.startPage(choice.getPageNo(), choice.getPageSize());
		List<Choice> list = super.findTeacherList(choice);
		PageInfo<Choice> pageInfo = new PageInfo<Choice>(list, choice.getPageSize());
		return pageInfo;
	}
	
	
	
	@Transactional(readOnly = false)
	public void save(Choice choice) {
		if (StringUtils.isEmpty(choice.getId())) {
			choice.setId(UUID.randomUUID().toString());
			dao.insert(choice);
		}else{
			dao.update(choice);
		}
	}
	
	@Transactional(readOnly = false)
	public void update(Choice choice) {
		dao.update(choice);
	}
	
	
	@Transactional(readOnly = false)
	public void delete(Choice choice) {
		super.delete(choice);
	}
	
	public List<Choice> dataList(String sid, String cid){
		return choicedao.dataList(sid, cid);
		
	}
	
	
	public List<Choice> dataLists(String tid, String cid){
		return choicedao.dataLists(tid, cid);
		
	}
	
	public List<Choice> dataStuList(String sid){
		return choicedao.dataStuList(sid);
	}
	
	
	
}