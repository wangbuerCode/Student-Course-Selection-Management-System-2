package com.demo.service;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Choice;
import com.demo.bean.Notice;
import com.demo.bean.Result;
import com.demo.dao.ChoiceDao;
import com.demo.dao.NoticeDao;
import com.demo.dao.ResultDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;


@Service
@Transactional(readOnly = true)
public class ResultService extends CrudService<ResultDao, Result> {

	
	@Autowired
	private ResultDao choicedao;
	
	
	public Result get(String id) {
		return super.get(id);
	}
	
	public List<Result> findList(Result result) {
		return super.findList(result);
	}
	
	public PageInfo<Result> findPage(Result result) {
		PageHelper.startPage(result.getPageNo(), result.getPageSize());
		List<Result> list = super.findList(result);
		PageInfo<Result> pageInfo = new PageInfo<Result>(list, result.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(Result result) {
		if (StringUtils.isEmpty(result.getId())) {
			result.setId(UUID.randomUUID().toString());
			dao.insert(result);
		}else{
			dao.update(result);
		}
	}
	
	@Transactional(readOnly = false)
	public void insert(Result result) {
			result.setId(UUID.randomUUID().toString());
			dao.insert(result);
	}
	
	@Transactional(readOnly = false)
	public void update(Result result) {
			dao.update(result);
	}
	
	
	@Transactional(readOnly = false)
	public void delete(Result result) {
		super.delete(result);
	}
	
	public List<Result> dataLists(String sid, String cid){
		return choicedao.dataList(sid, cid);
		
	}
	
	public Result del(String sid,String cid){
		return choicedao.del(sid,cid);
		
	}
	
}