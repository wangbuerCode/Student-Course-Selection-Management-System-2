package com.demo.service;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Active;
import com.demo.bean.Choice;
import com.demo.dao.ActiveDao;
import com.demo.dao.ChoiceDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 互动管理Service
 */
@Service
@Transactional(readOnly = true)
public class ActiveService extends CrudService<ActiveDao, Active> {


	@Autowired
	private ActiveDao choicedao;
	
	public Active get(String id) {
		return super.get(id);
	}
	
	public List<Active> findList(Active active) {
		return super.findList(active);
	}
	
	public PageInfo<Active> findPage(Active active) {
		PageHelper.startPage(active.getPageNo(), active.getPageSize());
		List<Active> list = super.findList(active);
		PageInfo<Active> pageInfo = new PageInfo<Active>(list, active.getPageSize());
		return pageInfo;
	}
	
	public PageInfo<Active> findPages(Active active) {
		PageHelper.startPage(active.getPageNo(), active.getPageSize());
		List<Active> list = super.findTeacherList(active);
		PageInfo<Active> pageInfo = new PageInfo<Active>(list, active.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(Active active) {
		if (StringUtils.isEmpty(active.getId())) {
			active.setId(UUID.randomUUID().toString());
			dao.insert(active);
		}else{
			dao.update(active);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Active active) {
		super.delete(active);
	}
	
	
	public List<Active> getByCid(String cid){
		return choicedao.getByCid(cid);
		
	}
	
}