package com.demo.service;

import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.bean.User;
import com.demo.common.CrudService;
import com.demo.dao.UserDao;

/**
 * 系统用户Service
 */
@Service
@Transactional(readOnly = true)
public class UserService extends CrudService<UserDao, User> {

	
	public User get(String id) {
		return super.get(id);
	}
	
	public List<User> findList(User user) {
		return super.findList(user);
	}
	
	public List<User> findTeacherList(User user){
		return dao.findTeacherList(user);
	}
	
	
	@Transactional(readOnly = false)
	public void delete(User user) {
		super.delete(user);
	}
	
	public int insert(User user) {
		user.setId(UUID.randomUUID().toString());
		return dao.insert(user);
	}
	
	public int update(User user) {
		return dao.update(user);
	}
	
	public int block(User user){
		return dao.block(user);
	}
	
	public User login(User user){
		return dao.login(user);
	}
}