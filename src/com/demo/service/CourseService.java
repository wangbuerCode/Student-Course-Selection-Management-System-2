package com.demo.service;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Course;
import com.demo.dao.CourseDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 *课程Service
 */
@Service
@Transactional(readOnly = true)
public class CourseService extends CrudService<CourseDao, Course> {

	public Course get(String id) {
		return super.get(id);
	}
	
	public List<Course> findList(Course course) {
		return super.findList(course);
	}
	
	public PageInfo<Course> findPage(Course course) {
		PageHelper.startPage(course.getPageNo(), course.getPageSize());
		List<Course> list = super.findList(course);
		PageInfo<Course> pageInfo = new PageInfo<Course>(list, course.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(Course course) {
		if (StringUtils.isEmpty(course.getId())) {
			course.setId(UUID.randomUUID().toString());
			dao.insert(course);
		}else{
			dao.update(course);
		}
	}
	
	@Transactional(readOnly = false)
	public void insert(Course course) {
		
			course.setId(UUID.randomUUID().toString());
			dao.insert(course);
		
	}
	
	@Transactional(readOnly = false)
	public void update(Course course) {
		
			dao.update(course);
	
	}
	
	@Transactional(readOnly = false)
	public void delete(Course course) {
		super.delete(course);
	}
	
}