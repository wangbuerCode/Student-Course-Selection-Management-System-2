/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.demo.bean.Menu;
import com.demo.common.CrudService;
import com.demo.dao.MenuDao;

/**
 * 系统菜单Service
 * @author s
 * @version 2017-05-05
 */
@Service
@Transactional(readOnly = true)
public class MenuService extends CrudService<MenuDao, Menu> {

	public Menu get(String id) {
		return super.get(id);
	}
	
	public List<Menu> findList(Menu menu) {
		return super.findList(menu);
	}
	
	/*public Page<Menu> findPage(Page<Menu> page, Menu menu) {
		return super.findPage(page, menu);
	}
	
	@Transactional(readOnly = false)
	public void save(Menu menu) {
		super.save(menu);
	}*/
	
	@Transactional(readOnly = false)
	public void delete(Menu menu) {
		super.delete(menu);
	}
	
	
	
	public List<Menu> getByFjId(String fjId){
		return dao.getByFjId(fjId);
	}
	
	/**
	 * 获取菜单数据
	 * @return
	 */
	public List<Map<String, Object>> getMenu(){
		List<Menu> byFjId = dao.getByFjId("0");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (Menu menu : byFjId) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("parentMenu", menu);
			String id = menu.getId();
			List<Menu> childMenus = dao.getByFjId(id);
			map.put("childMenus", childMenus);
			list.add(map);
		}
		return list;
	}

	public int insert(Menu menu) {
		menu.setId(UUID.randomUUID().toString());
		return dao.insert(menu);
	}

	public int update(Menu menu) {
		return dao.update(menu); 
	}
	
}