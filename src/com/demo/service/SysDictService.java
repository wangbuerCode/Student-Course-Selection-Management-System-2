/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.common.CrudService;
import com.demo.bean.SysDict;
import com.demo.dao.SysDictDao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 字典管理Service
 * @author demo_cxy
 *
 */
@Service
@Transactional(readOnly = true)
public class SysDictService extends CrudService<SysDictDao, SysDict> {

	public SysDict get(String id) {
		return super.get(id);
	}
	
	public List<SysDict> findList(SysDict sysDict) {
		return super.findList(sysDict);
	}
	
	public PageInfo<SysDict> findPage(SysDict sysDict) {
		PageHelper.startPage(sysDict.getPageNo(), sysDict.getPageSize());
		List<SysDict> list = super.findList(sysDict);
		PageInfo<SysDict> pageInfo = new PageInfo<SysDict>(list, sysDict.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(SysDict sysDict) {
		if ("0".equals(sysDict.getId())) {
			sysDict.setId(UUID.randomUUID().toString());
			dao.insert(sysDict);
		}else{
			dao.update(sysDict);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(SysDict sysDict) {
		super.delete(sysDict);
	}
	
}