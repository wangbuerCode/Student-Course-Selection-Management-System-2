package com.demo.service;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.demo.common.CrudService;
import com.demo.bean.Active;
import com.demo.bean.Reply;
import com.demo.bean.Result;
import com.demo.dao.ActiveDao;
import com.demo.dao.ReplyDao;
import com.demo.dao.ResultDao;
import com.demo.utils.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 回复Service
 */
@Service
@Transactional(readOnly = true)
public class ReplyService extends CrudService<ReplyDao, Reply> {

	@Autowired
	private ReplyDao choicedao;
	public Reply get(String id) {
		return super.get(id);
	}
	
	public List<Reply> findList(Reply reply) {
		return super.findList(reply);
	}
	
	public PageInfo<Reply> findPage(Reply reply) {
		PageHelper.startPage(reply.getPageNo(), reply.getPageSize());
		List<Reply> list = super.findList(reply);
		PageInfo<Reply> pageInfo = new PageInfo<Reply>(list, reply.getPageSize());
		return pageInfo;
	}
	
	
	@Transactional(readOnly = false)
	public void save(Reply reply) {
		if (StringUtils.isEmpty(reply.getId())) {
			reply.setId(UUID.randomUUID().toString());
			dao.insert(reply);
		}else{
			dao.update(reply);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Reply reply) {
		super.delete(reply);
	}
	
	
	public List<Reply> dataLists(String sid, String cid){
		return choicedao.dataList(sid, cid);
		
	}
	
}