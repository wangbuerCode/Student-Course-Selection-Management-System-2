/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.bean.Role;
import com.demo.common.CrudService;
import com.demo.dao.RoleDao;

/**
 * 系统角色Service
 * @author s
 * @version 2017-05-06
 */
@Service
@Transactional(readOnly = true)
public class RoleService extends CrudService<RoleDao, Role> {

	@Autowired
	RoleDao roleDao;
	
	public Role get(String id) {
		return super.get(id);
	}
	
	public List<Role> findList(Role role) {
		return super.findList(role);
	}
	
	/*public Page<Role> findPage(Page<Role> page, Role role) {
		return super.findPage(page, role);
	}
	
	@Transactional(readOnly = false)
	public void save(Role role) {
		super.save(role);
	}*/
	
	@Transactional(readOnly = false)
	public void delete(Role role) {
		super.delete(role);
	}
	
	@Transactional(readOnly = false)
	public int insert(Role role){
		role.setId(UUID.randomUUID().toString());
		return dao.insert(role);
	}
	
	public int update(Role role){
		return dao.update(role);
	}
	
}