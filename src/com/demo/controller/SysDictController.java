/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.bean.SysDict;
import com.demo.service.SysDictService;

/**
 * 字典管理Controller
 * @author demo_cxy
 *
 */
@Controller
@RequestMapping(value = "/demo/sysDict")
public class SysDictController{

	@Autowired
	private SysDictService sysDictService;
	
	@ModelAttribute
	public SysDict get(@RequestParam(required=false) String id) {
		SysDict entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = sysDictService.get(id);
		}
		if (entity == null){
			entity = new SysDict();
			entity.setId("0");
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(SysDict sysDict, HttpServletRequest request, HttpServletResponse response, Model model) {
		PageInfo<SysDict> page = sysDictService.findPage(sysDict);
		model.addAttribute("pageInfo", page);
		return "/sys/sysDictList";
	}

	@RequestMapping(value = "form")
	public String form(SysDict sysDict, Model model) {
		model.addAttribute("sysDict", sysDict);
		return "/sys/sysDictForm";
	}

	@RequestMapping(value = "save")
	public String save(SysDict sysDict, Model model, RedirectAttributes redirectAttributes) {
		sysDictService.save(sysDict);
		return "redirect:"+"/demo/sysDict";
	}
	
	@RequestMapping(value = "delete")
	public String delete(SysDict sysDict, RedirectAttributes redirectAttributes) {
		sysDictService.delete(sysDict);
		/*addMessage(redirectAttributes, "删除字典管理成功");*/
		return "redirect:"+"/demo/sysDict";
	}

}