package com.demo.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Choice;
import com.demo.bean.Course;
import com.demo.bean.Result;
import com.demo.bean.User;
import com.demo.service.ChoiceService;
import com.demo.service.CourseService;
import com.demo.service.ResultService;
import com.demo.service.UserService;

/**
 * 课程Controller
 */
@Controller
@RequestMapping(value = "/course")
public class CourseController{

	@Autowired
	private CourseService courseService;
	@Autowired
	UserService userService;
	@Autowired
	private ChoiceService choiceService;
	@Autowired
	private ResultService resultService;
	
	@ModelAttribute
	public Course get(@RequestParam(required=false) String id) {
		Course entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = courseService.get(id);
		}else{
			entity = new Course();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(Course course, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Course> page = null;
		/*
		 * if(user.getRoleId().equals("3")||user.getRoleId().equals("4")) { page =
		 * courseService.findPage(course); }else { page =
		 * courseService.findPage(course); }
		 */
		page = courseService.findPage(course);
		model.addAttribute("pageInfo", page);
		return "/demo/courseList";
	}
	
	@RequestMapping(value = "look")
	public String form(Course course, Model model) {
		model.addAttribute("course", course);
		return "/demo/look";
	}

	@RequestMapping(value = "form")
	public String form(Course course, User user,Model model,HttpServletRequest request) {
		//User user = UserUtils.getCurrentUser(request);
		List<User> users = userService.findTeacherList(user);
		model.addAttribute("users",users);
		model.addAttribute("course", course);
		return "/demo/courseForm";
	}

	@RequestMapping(value = "save")
	public String save(Course course, Model model, RedirectAttributes redirectAttributes) {		
		if (course.getStimeStr()!=null && course.getStimeStr()!="") {
			Timestamp birthday = Timestamp.valueOf(course.getStimeStr()+" 00:00:00");
			course.setStime(birthday);
		}
		if (course.getEtimeStr()!=null && course.getEtimeStr()!="") {
			Timestamp birthday = Timestamp.valueOf(course.getEtimeStr()+" 00:00:00");
			course.setEtime(birthday);
		}
		if (StringUtils.isEmpty(course.getId())) {
			course.setId(UUID.randomUUID().toString());
			courseService.insert(course);
			
			/* 添加课程并加入选课表 */
			Choice choice = new Choice();
			choice.setCid(course.getId());
			choice.setTid(course.getCteacher());
			choiceService.save(choice);
			
		}else{
			courseService.update(course);
		}
		redirectAttributes.addFlashAttribute("message", "保存课程信息成功！");
		return "redirect:"+"/course";
	}
	
	@RequestMapping(value = "selectCourse")
	public String selectCourse(Course course, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {		
			User user = UserUtils.getCurrentUser(request);
			String stuId = user.getId();
			String courseId = course.getId();
			List<Choice> dataList =choiceService.dataList(stuId, courseId);
			if(dataList.size()>0) {
				redirectAttributes.addFlashAttribute("message", "已经选择了该课程，请重新选择！");
			}else {
				/* 添加课程并加入选课表 */
				Choice choice = new Choice();
				choice.setCid(course.getId());
				choice.setSid(user.getId());
				choice.setTid(course.getCteacher());
				choiceService.save(choice);
				redirectAttributes.addFlashAttribute("message", "选择课程信息成功！");
				/* 添加课程并加入成绩信息 */
				Result result = new Result();
				result.setSid(stuId);
				result.setCid(courseId);
				result.setUndefine(course.getCteacher());
				resultService.save(result);
				
			}
			
			return "redirect:"+"/course";
	}
	
	
	
	@RequestMapping(value = "delete")
	public String delete(Course course, RedirectAttributes redirectAttributes) {
		courseService.delete(course);
		return "redirect:"+"/course";
	}

}