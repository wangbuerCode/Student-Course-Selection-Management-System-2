package com.demo.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Active;
import com.demo.bean.Reply;
import com.demo.bean.Result;
import com.demo.bean.User;
import com.demo.service.ActiveService;
import com.demo.service.ReplyService;
import com.demo.service.ResultService;
import com.demo.service.UserService;

/**
 * 回复SController
 */
@Controller
@RequestMapping(value = "/reply")
public class ReplyController {

	@Autowired
	private ReplyService replyService;
	@Autowired
	UserService userService;
	@Autowired
	private ResultService resultService;

	@ModelAttribute
	public Reply get(@RequestParam(required = false) String id) {
		Reply entity = null;
		if (StringUtils.isNotBlank(id)) {
			entity = replyService.get(id);
		} else {
			entity = new Reply();
		}
		return entity;
	}

	@RequestMapping(value = { "list", "" })
	public String list(Reply reply, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user", user);
		PageInfo<Reply> page = null;
		page = replyService.findPage(reply);
		model.addAttribute("pageInfo", page);
		return "/demo/replyList";
	}

	@RequestMapping(value = "infolook")
	public String form(Reply reply, Model model) {
		model.addAttribute("reply", reply);
		return "/demo/infoLook";
	}

	@RequestMapping(value = "form")
	public String form(Reply reply, User user, Model model, HttpServletRequest request) {
		List<User> users = userService.findTeacherList(user);
		model.addAttribute("users", users);
		model.addAttribute("reply", reply);
		return "/demo/replyForm";
	}

	@RequestMapping(value = "save")
	public String save(Reply reply, Model model, String atime, RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		User user = UserUtils.getCurrentUser(request);
		String aid = request.getParameter("aid");
		String tid = request.getParameter("tid");
		String atimes = request.getParameter("atime"); // 互动开始时间
		String cid = request.getParameter("cid");
		reply.setAid(aid);
		reply.setTid(tid);
		reply.setUndefine(cid);
		reply.setSid(user.getId());
		reply.setRtime(new Date());
		replyService.save(reply);
		/*
		 * 回复后保存成绩信息 Result result = new Result(); if(user.getRoleId().equals("4")) {
		 * result.setCid(cid);// 课程 result.setSid(user.getId());//学生 Integer res =0; //
		 * try { Date d1 = df.parse(df.format(reply.getAtime())); Date d2 =
		 * df.parse(df.format(reply.getRtime())); Integer diff = (int) (d2.getTime() -
		 * d1.getTime());//这样得到的差值是微秒级别 Integer days = diff / (1000 * 60 * 60 * 24);
		 * Integer hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60); Integer
		 * minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
		 * Integer s = (diff / 1000 - days * 24 * 60 * 60 - hours * 60 * 60 - minutes *
		 * 60); if(minutes <= 1) { res =50; result.setResult(res); }else if(minutes <=
		 * 2) { res = 30; result.setResult(res); }else if(minutes <= 3) { res = 10 ;
		 * result.setResult(res); }else { res = 0; result.setResult(res); } } catch
		 * (ParseException e) { } 判断是否已经存在成绩 List<Result> dataList =
		 * resultService.dataLists(user.getId(), cid);// 成绩 List<Reply> replyList =
		 * replyService.dataLists(user.getId(), cid);// 回复数量 Result newResult = new
		 * Result(); if(replyList.size()==1) { result.setResult(10);
		 * resultService.insert(result); }else if(replyList.size()==2) { newResult =
		 * resultService.get(dataList.get(0));
		 * newResult.setResult(dataList.get(0).getResult()+20);
		 * resultService.update(newResult); }else if(replyList.size()==3) { newResult =
		 * resultService.get(dataList.get(0));
		 * newResult.setResult(dataList.get(0).getResult()+20);
		 * resultService.update(newResult); } else { result.setResult(10);
		 * resultService.insert(result); }
		 * 
		 * }
		 */
		return "redirect:" + "/active/infolook?id=" + aid;
	}

	@RequestMapping(value = "delete")
	public String delete(Reply reply, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		/*
		 * List<Reply> replyList = replyService.dataLists(reply.getSid(),
		 * reply.getUndefine());// 回复数量 List<Result> dataList =
		 * resultService.dataLists(reply.getSid(), reply.getUndefine());// 成绩 if
		 * (replyList.size() > 0 && replyList.size() <= 3) { Integer result =
		 * dataList.get(0).getResult(); if (result > 20) {
		 * dataList.get(0).setResult(dataList.get(0).getResult() - 20);
		 * resultService.update(dataList.get(0)); replyService.delete(reply); } else {
		 * replyService.delete(reply); } }
		 */
		replyService.delete(reply);
		return "redirect:" + "/active/infolook?id=" + reply.getAid();
	}

}