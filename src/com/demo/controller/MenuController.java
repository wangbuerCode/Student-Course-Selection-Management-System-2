package com.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.bean.Authority;
import com.demo.bean.Menu;
import com.demo.bean.Msg;
import com.demo.bean.User;
import com.demo.dao.AuthorityDao;
import com.demo.service.MenuService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(value = "/system/menu")
public class MenuController {

	@Autowired
	MenuService menuService;
	@Autowired
	AuthorityDao authorityDao;
	
	@ModelAttribute
	public Menu get(@RequestParam(required=false) String id) {
		Menu entity = null;
		if (id!=null && id!=""){
			entity = menuService.get(id);
		}
		if (entity == null){
			entity = new Menu();
		}
		return entity;
	}
	/**
	 * 获取左侧窗口菜单
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLeftMenu")
	public String getMenu(Model model,HttpServletRequest request){
		Object attribute = request.getSession().getAttribute("currentUser");
		User currentUser = new User();
		if (attribute!=null && attribute instanceof User) {
			currentUser = (User)attribute;
			String roleId = currentUser.getRoleId();
			List<Authority> byRoleId = authorityDao.getByRoleId(roleId);
			model.addAttribute("authorityList", byRoleId);
		}
		List<Map<String, Object>> list = menuService.getMenu();
		model.addAttribute("list", list);
		return "sys/left";
	}
	
	/**
	 * 返回所有的父级菜单
	 */
	@RequestMapping("/menuJson")
	@ResponseBody
	public Msg getParentMenu(@RequestParam(value = "id", defaultValue = "0") Integer id){
		Menu menu = new Menu();
		if (id!=0) {
			menu = menuService.get(""+id);
		}
		//查出的所有父级菜单
		List<Menu> byFjId = menuService.getByFjId("0");
		return Msg.success().add("depts", byFjId).add("menu", menu);
	}
	
	/**
	 * json格式返回所有的菜单 
	 */
	@RequestMapping("/menusJson")
	@ResponseBody
	public Msg getMenuJson(@RequestParam(value = "id", defaultValue = "0") Integer id){
		Menu menu = new Menu();
		//查出的所有菜单
		List<Menu> byFjId = menuService.findList(menu);
		//拼接成json格式的数据 { id:2, pId:0, name:"随意勾选 2", checked:true, open:true}
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		for (Menu menu2 : byFjId) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", menu2.getId());
			map.put("pId", menu2.getFjId());
			map.put("name", menu2.getName());
			
			if ("0".equals(menu2.getFjId())) {
				map.put("open", "true");
				map.put("checked", "true");
			}
			list.add(map);
		}
		return Msg.success().add("list", list);
	}
	
	/**
	 * 添加
	 * @return
	 */
	@RequestMapping("/save")
	public String add(Menu menu,Model model){
		if (menu.getId()!=null && !"".equals(menu.getId())) {
			menuService.update(menu);
		}else {
			menuService.insert(menu);
		}
		return "redirect:/system/menu/list";
	}
	
	@RequestMapping("/del")
	public String del(String id){
		Menu menu = new Menu();
		menu.setId(id);
		menuService.delete(menu);
		return "redirect:/system/menu/list";
	}
	
	/**
	 * 查询菜单数据（分页查询）
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String getEmps(Menu menu, Model model) {
		// 这不是一个分页查询；
		// 引入PageHelper分页插件
		// 在查询之前只需要调用，传入页码，以及每页的大小
		PageHelper.startPage(menu.getPageNo(), menu.getPageSize());
		// startPage后面紧跟的这个查询就是一个分页查询
		List<Menu> menus = menuService.findList(menu);
		// 使用pageInfo包装查询后的结果，只需要将pageInfo交给页面就行了。
		// 封装了详细的分页信息,包括有我们查询出来的数据，传入连续显示的页数
		PageInfo page = new PageInfo(menus, menu.getPageSize());
		model.addAttribute("pageInfo", page);
		return "sys/menuList";
	}
	
	/**
	 * 更新
	 * @return
	 */
	@RequestMapping("/update")
	public String update(int id, String name,String url,int sort,String fjId){
		Menu menu = menuService.get(""+id);
		menu.setName(name);
		menu.setUrl(url);
		menu.setSort(sort);
		menu.setFjId(fjId);
		if (!"0".equals(fjId)) {
			menu.setFjIds("0,"+fjId);
		}else {
			menu.setFjIds("0,");
		}
		menuService.update(menu);
		return "redirect:/system/menu/list";
	}
	
	@RequestMapping("/form")
	public String form(Menu menu,Model model){
		model.addAttribute("menu", menu);
		//查出的所有父级菜单
		List<Menu> byFjId = menuService.getByFjId("0");
		model.addAttribute("menus", byFjId);
		return "sys/sysMenuForm";
	}
}
