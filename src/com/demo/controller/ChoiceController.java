package com.demo.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Choice;
import com.demo.bean.Course;
import com.demo.bean.Result;
import com.demo.bean.User;
import com.demo.service.ChoiceService;
import com.demo.service.CourseService;
import com.demo.service.ResultService;
import com.demo.service.UserService;

/**
 * Controller
 */
@Controller
@RequestMapping(value = "/choice")
public class ChoiceController{

	@Autowired
	private ChoiceService choiceService;
	@Autowired
	UserService userService;
	@Autowired
	private ResultService resultService;
	
	@ModelAttribute
	public Choice get(@RequestParam(required=false) String id) {
		Choice entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = choiceService.get(id);
		}else{
			entity = new Choice();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(Choice choice, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Choice> page = null;
		if(user.getRoleId().equals("3")) {
			choice.setTid(user.getId());
			page = choiceService.findPage(choice);
		}else if(user.getRoleId().equals("4")) {
			choice.setSid(user.getId());
			page = choiceService.findPage(choice);
		}else {
			page = choiceService.findPage(choice);
		}
		model.addAttribute("pageInfo", page);
		return "/demo/choiseList";
	}
	
	@RequestMapping(value = {"seleChoice"})
	public String seleChoice(Choice choice, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Choice> page = null;
		
		if(user.getRoleId().equals("3")) {
			choice.setTid(user.getId());
			page = choiceService.findPages(choice);
		}else {
			page = choiceService.findPages(choice);
		}
		model.addAttribute("pageInfo", page);
		return "/demo/choiceList";
	}
	
	
	
	@RequestMapping(value = "look")
	public String form(Choice choice, Model model) {
		model.addAttribute("choice", choice);
		return "/demo/look";
	}

	@RequestMapping(value = "form")
	public String form(Choice choice, User user,Model model,HttpServletRequest request) {
		//User user = UserUtils.getCurrentUser(request);
		List<User> users = userService.findTeacherList(user);
		model.addAttribute("users",users);
		model.addAttribute("choice", choice);
		return "/demo/choiceForm";
	}

	@RequestMapping(value = "save")
	public String save(Choice choice, Model model, RedirectAttributes redirectAttributes) {		
		
		choiceService.save(choice);
		return "redirect:"+"/choice";
	}
	
	@RequestMapping(value = "delete")
	public String delete(Choice choice, Result result,RedirectAttributes redirectAttributes) {
		choiceService.delete(choice);
		result = resultService.del(choice.getSid(),choice.getCid());
		resultService.delete(result);
		redirectAttributes.addFlashAttribute("message", "删除成功");
		return "redirect:"+"/choice";
	}

}