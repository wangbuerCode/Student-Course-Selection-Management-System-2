package com.demo.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Notice;
import com.demo.bean.Result;
import com.demo.bean.User;
import com.demo.service.NoticeService;
import com.demo.service.ResultService;

@Controller
@RequestMapping(value = "/result")
public class ResultController{

	@Autowired
	private ResultService resultService;
	
	@ModelAttribute
	public Result get(@RequestParam(required=false) String id) {
		Result entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = resultService.get(id);
		}else{
			entity = new Result();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(Result result, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Result> page = null;
		if(user.getRoleId().equals("3")) {
			result.setUndefine(user.getId());
			page = resultService.findPage(result);
		}
		if(user.getRoleId().equals("4")) {
			result.setSid(user.getId());
			page = resultService.findPage(result);
		}else {
			page = resultService.findPage(result);
		}
		model.addAttribute("pageInfo", page);
		return "/demo/resultList";
	}
	
	

	@RequestMapping(value = "form")
	public String form(Result result, Model model,HttpServletRequest request) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		model.addAttribute("result", result);
		return "/demo/resultForm";
	}

	@RequestMapping(value = "save")
	public String save(Result result, Model model, RedirectAttributes redirectAttributes) {
		resultService.save(result);
		redirectAttributes.addFlashAttribute("message", "评分成功!");
		return "redirect:"+"/result";
	}
	
	@RequestMapping(value = "delete")
	public String delete(Result result, RedirectAttributes redirectAttributes) {
		resultService.delete(result);
		return "redirect:"+"/result";
	}

}