package com.demo.controller;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.bean.Authority;
import com.demo.bean.Menu;
import com.demo.bean.Msg;
import com.demo.bean.Role;
import com.demo.dao.AuthorityDao;
import com.demo.service.MenuService;
import com.demo.service.RoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping(value = "/system/role")
public class RoleController {
	
	@Autowired
	RoleService	roleService;
	
	@Autowired
	MenuService menuService;
	@Autowired
	AuthorityDao authorityDao;
	
	@ModelAttribute
	public Role get(@RequestParam(required=false) String id) {
		Role entity = null;
		if (id!=null && id!=""){
			entity = roleService.get(id);
		}
		if (entity == null){
			entity = new Role();
		}
		return entity;
	}
	
	/**
	 * 添加
	 * @return
	 */
	@RequestMapping("/save")
	public String add(String name){
		Role role = new Role();
		role.setName(name);
		roleService.insert(role);
		return "redirect:/system/role/list";
	}
	
	@RequestMapping("/del")
	public String del(String id){
		Role role = new Role();
		role.setId(id);
		roleService.delete(role);
		return "redirect:/system/role/list";
	}
	
	@RequestMapping("/update")
	public String update(String id,String name){
		Role role = roleService.get(id);
		role.setName(name);
		roleService.update(role);
		return "redirect:/system/role/list";
	}
	
	/**
	 * 查询部门数据（分页查询）
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String getDepts( Role role, Model model) {
		// 这不是一个分页查询；
		// 引入PageHelper分页插件
		// 在查询之前只需要调用，传入页码，以及每页的大小
		PageHelper.startPage(role.getPageNo(), role.getPageSize());
		// startPage后面紧跟的这个查询就是一个分页查询
		List<Role> roles = roleService.findList(role);
		// 使用pageInfo包装查询后的结果，只需要将pageInfo交给页面就行了。
		// 封装了详细的分页信息,包括有我们查询出来的数据，传入连续显示的页数
		PageInfo page = new PageInfo(roles, role.getPageSize());
		model.addAttribute("pageInfo", page);
		return "sys/sysRoleList";
	}

	/**
	 * 返回所有的父级菜单
	 */
	@RequestMapping("/roleJson")
	@ResponseBody
	public Msg getParentMenu(@RequestParam(value = "id", defaultValue = "1") String id){
		//获取角色
		Role role = roleService.get(id);
		return Msg.success().add("role", role);
	}
	
	@RequestMapping("/setAutory")
	public String setAutorty(Model model,String roleId){
		List<Map<String, Object>> list = menuService.getMenu();
		model.addAttribute("list", list);
		model.addAttribute("roleId", roleId);
		List<String> byRoleId = authorityDao.getMenuIds(roleId);
		model.addAttribute("roldIds", byRoleId);
		return "sys/autority";
	}
	
	@RequestMapping("/saveAutory")
	@ResponseBody
	public Msg saveAutorty(String str,String roleId){
		//更新权限之前要先删除再更新
		List<Authority> byRoleId = authorityDao.getByRoleId(roleId);
		for (Authority authority : byRoleId) {
			authorityDao.delete(authority);
		}
		Authority authority = new Authority();
		authority.setRoleId(roleId);
		String[] split = str.split(",");
		for (String string : split) {
			authority.setId(UUID.randomUUID().toString());
			authority.setMenuId(string);
			authorityDao.insert(authority);
		}
		return Msg.success();
	}
}
