package com.demo.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.bean.Msg;
import com.demo.bean.Notice;
import com.demo.bean.Role;
import com.demo.bean.User;
import com.demo.service.NoticeService;
import com.demo.service.RoleService;
import com.demo.service.UserService;
import com.demo.utils.FileUploadAndDowload;
import com.demo.utils.Global;
import com.demo.utils.MD5;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.demo.utils.excel.ImportExcel;
import com.demo.utils.excel.ExportExcel;

@Controller
@RequestMapping(value = "/system/user")
public class UserController {
	
	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	@Autowired
	private NoticeService noticeService;
	
	@ModelAttribute
	public User get(@RequestParam(required=false) String id) {
		User entity = null;
		if (id!=null && id!=""){
			entity = userService.get(id);
		}
		if (entity == null){
			entity = new User();
		}
		return entity;
	}
	
	/**
	 * 跳转到登录界面
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String toLogin(){
		return "sys/login";
	}
	
	@RequestMapping(value = "/toTop")
	public String toTop(HttpServletRequest request,Model model){
		return "sys/top";
	}
	@RequestMapping(value = "/toLeft")
	public String toLeft(){
		return "sys/left";
	}
	@RequestMapping(value = "/toIndex")
	public String toIndex(Model model,Notice notice){
		PageInfo<Notice> page= noticeService.findPage(notice);
		model.addAttribute("pageInfo", page);
		return "sys/index";
	}
	
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.invalidate();//销毁session对象
		return "sys/login";
	}
	
	/**
	 * 获取个人信息
	 * @param user
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userInfo")
	public String getUserInfo(User user,HttpServletRequest request,Model model){
		user = (User) request.getSession().getAttribute("currentUser");
		System.err.println(user.getTrueName());
		model.addAttribute("user", user);
		//获取角色部门数据
		/*
		 * List<Dept> deptList = deptService.findList(new Dept());
		 * model.addAttribute("depts", deptList);
		 */
		return "sys/userInfo";
	}
	
	/**
	 * 跳转到修改密码页面
	 * @param user
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/toSetPass")
	public String toSetPass(User user,HttpServletRequest request,Model model){
		user = UserUtils.getCurrentUser(request);
		model.addAttribute("user", user);
		return "sys/reSetPass";
	}
	
	/**
	 * 修改密码
	 * @param user
	 * @param oldPassword
	 * @param new1Password
	 * @param new2Password
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/setPassword")
	public String setPassword(User user,String oldPassword,String new1Password,String new2Password,HttpServletRequest request,Model model){
		String message = "";//设置提示信息
		String type = "0";//设置修改状态 默认失败
		model.addAttribute("oldPassword", oldPassword);
		model.addAttribute("new1Password", new1Password);
		model.addAttribute("new2Password", new2Password);
		String password = user.getPassword();
		String old = MD5.Encrypt(oldPassword);
		if (StringUtils.isEmpty(oldPassword)) {
			message = "请输入旧密码";
		}else if (StringUtils.isEmpty(new1Password)) {
			message = "请输入新密码";
		}else if (StringUtils.isEmpty(new2Password)) {
			message = "请输入确认密码";
		}else if (!password.equals(old)) {
			message = "旧密码验证不通过";
		}else if (!new1Password.equals(new2Password)) {
			message = "两次密码不一致";
		}else if (password.equals(MD5.Encrypt(new1Password))) {
			message = "新密码不能与旧密码相同";
		}else {
			password = MD5.Encrypt(new1Password);
			user.setPassword(password);
			userService.update(user);
			message = "修改成功";
			type="1";
		}
		model.addAttribute("message", message);
		model.addAttribute("type", type);
		return "sys/reSetPass";
	}
	
	@RequestMapping(value = "/returnPassword")
	public String returnPassword(User user,RedirectAttributes redirectAttributes,Model model){
		user.setPassword(MD5.Encrypt("123456"));
		userService.update(user);
		redirectAttributes.addFlashAttribute("message", user.getUsername()+"账号密码重置成功，初始密码为123456！");
		return "redirect:/system/user/list";
	}
	
	
	@RequestMapping(value = "/login")
	public String login(User user,HttpServletRequest request,Model model){
		if (user.getUsername()==null || user.getPassword() == null) {
			model.addAttribute("msg", "用户名密码不能为空！");
			return "sys/login";
		}
		String password = user.getPassword();
		user.setPassword(MD5.Encrypt(password));
		User login = userService.login(user);
		if (login!=null) {
			if ("1".equals(login.getIsBlock())) {
				model.addAttribute("username", user.getUsername());
				model.addAttribute("password", password);
				model.addAttribute("msg", "用户名已锁定，请联系管理员！");
				return "sys/login";
			}else{
				System.err.println("登录成功");
				HttpSession session = request.getSession();
				session.setAttribute("currentUser", login);
				session.setAttribute("productName", Global.getConfig("productName"));
				return "sys/main";
			}
		}else {
			System.out.println("登录失败");
			model.addAttribute("username", user.getUsername());
			model.addAttribute("password", password);
			model.addAttribute("msg", "用户名或密码错误！");
			return "sys/login";
		}
	}
	
	
	@RequestMapping("/del")
	public String del(String id){
		User user = new User();
		user.setId(id);
		userService.delete(user);
		return "redirect:/system/user/list";
	}
	
	@RequestMapping("/block")
	public String block(String id,String isBlock,RedirectAttributes redirectAttributes){
		User user = new User();
		user.setId(id);
		user.setIsBlock(isBlock);
		userService.block(user);
		redirectAttributes.addFlashAttribute("message", "解锁成功！");
		return "redirect:/system/user/list";
	}
	
	/**
	 * 添加用户
	 * @param username
	 * @param password
	 * @param birth
	 * @param trueName
	 * @param email
	 * @param sex
	 * @param address
	 * @param phone
	 * @param roleId
	 * @param deptId
	 * @return
	 */
	@RequestMapping("/regit")
	public String regit(
			@RequestParam(value = "userNo", defaultValue = "0000") String userNo,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "password", defaultValue = "1") String password,
			@RequestParam(value = "birth", defaultValue = "1") String birth,
			@RequestParam(value = "trueName", defaultValue = "1") String trueName,
			@RequestParam(value = "email", defaultValue = "1") String email,
			@RequestParam(value = "sex", defaultValue = "1") String sex,
			@RequestParam(value = "address", defaultValue = "1") String address,
			@RequestParam(value = "phone", defaultValue = "1") String phone,
			@RequestParam(value = "roleId", defaultValue = "1") String roleId,
			@RequestParam(value = "deptId", defaultValue = "1") String deptId,
			Model model
			){
		User user = new User();
		user.setUserNo(userNo);
		user.setUsername(username);
		password = MD5.Encrypt(password);//加密
		user.setPassword(password);
		Timestamp birthday = Timestamp.valueOf(birth+" 00:00:00");
		user.setBirth(birthday);
		user.setTrueName(trueName);
		user.setEmail(email);
		user.setSex(sex);
		user.setAddress(address);
		user.setPhone(phone);
		user.setIsBlock("1");// 默认注册账号不能使用
		user.setDelFlag(0);
		user.setRoleId(roleId);
		user.setDeptId(deptId);
		//如果id存在 则执行更新操作，否则添加记录。
		userService.insert(user);
		model.addAttribute("msg", "注册成功！");
		return "sys/login";
	}
	
	/**
	 * 查询员工数据（分页查询）
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public String getEmps(User user, Model model) {
		// 这不是一个分页查询；
		// 引入PageHelper分页插件
		// 在查询之前只需要调用，传入页码，以及每页的大小
		PageHelper.startPage(user.getPageNo(), user.getPageSize());
		// startPage后面紧跟的这个查询就是一个分页查询
		List<User> users = userService.findList(user);
		// 使用pageInfo包装查询后的结果，只需要将pageInfo交给页面就行了。
		// 封装了详细的分页信息,包括有我们查询出来的数据，传入连续显示的页数
		PageInfo page = new PageInfo(users, user.getPageSize());
		model.addAttribute("pageInfo", page);
		return "sys/sysUserList";
	}
	
	/**
	 * 返回所有的角色，部门信息
	 */
	@RequestMapping("/form")
	public String form(User user, Model model){
		model.addAttribute("user", user);
		List<Role> roleList = roleService.findList(new Role());
		model.addAttribute("roles", roleList);
		return "sys/sysUserForm";
	}
	
	/**
	 * 返回所有的角色，部门信息
	 */
	@RequestMapping("/getJson")
	@ResponseBody
	public Msg getRoleDept(@RequestParam(value = "id", defaultValue = "0") Integer id){
		User user = new User();
		if (id!=0) {
			user = userService.get(""+id);
		}
		List<Role> roleList = roleService.findList(new Role());
		return Msg.success().add("roleList", roleList).add("user", user);
	}
	
	@RequestMapping("/save")
	public String save(User user, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
		if (user.getBirthstr()!=null && user.getBirthstr()!="") {
			Timestamp birthday = Timestamp.valueOf(user.getBirthstr()+" 00:00:00");
			user.setBirth(birthday);
		}
		//如果id存在 则执行更新操作，否则添加记录。
		if (user.getId()!=""&&user.getId()!=null) {
			userService.update(user);
		}else{
			//添加时设置默认密码
			user.setIsBlock("0");
			user.setDelFlag(0);
			String password = MD5.Encrypt("123456");//加密
			user.setPassword(password);
			userService.insert(user);
		}
		redirectAttributes.addFlashAttribute("message", "保存用户成功");
		return "redirect:/system/user/list";
	}
	
	@RequestMapping("/fixInfo")
	public String fixInfo(User user, Model model, RedirectAttributes redirectAttributes,@RequestParam(value = "file",required=false) MultipartFile file,HttpServletRequest request) {
		/* String url = FileUploadAndDowload.upload(file,request); */
		/*
		 * System.out.println(url); user.setUrl(url);
		 */
		if (user.getBirthstr()!=null && user.getBirthstr()!="") {
			Timestamp birthday = Timestamp.valueOf(user.getBirthstr()+" 00:00:00");
			user.setBirth(birthday);
		}
		//如果id存在 则执行更新操作，否则添加记录。
		if (user.getId()!=""&&user.getId()!=null) {
			userService.update(user);
		}else{
			//添加时设置默认密码
			String password = MD5.Encrypt("123456");//加密
			user.setPassword(password);
			userService.insert(user);
		}
		model.addAttribute("message", "修改个人信息成功，页面有缓存请刷新页面重新加载。");
		return "/sys/userInfo";
	}
}
