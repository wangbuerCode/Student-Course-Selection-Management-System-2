package com.demo.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Notice;
import com.demo.bean.User;
import com.demo.service.NoticeService;

/**
 * 系统公告Controller
 */
@Controller
@RequestMapping(value = "/notice")
public class NoticeController{

	@Autowired
	private NoticeService noticeService;
	
	@ModelAttribute
	public Notice get(@RequestParam(required=false) String id) {
		Notice entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = noticeService.get(id);
		}else{
			entity = new Notice();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(Notice notice, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Notice> page = null;
		if(user.getRoleId().equals("3")) {
			notice.setType("1");
			page= noticeService.findPage(notice);
		}else {
			page = noticeService.findPage(notice);
		}
		
		model.addAttribute("pageInfo", page);
		return "/demo/noticeList";
	}
	
	@RequestMapping(value = "look")
	public String form(Notice notice, Model model) {
		model.addAttribute("notice", notice);
		return "/demo/look";
	}

	@RequestMapping(value = "form")
	public String form(Notice notice, Model model,HttpServletRequest request) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		model.addAttribute("notice", notice);
		return "/demo/noticeForm";
	}

	@RequestMapping(value = "save")
	public String save(Notice notice, Model model, RedirectAttributes redirectAttributes) {
		notice.setAddTime(new Date());
		noticeService.save(notice);
		redirectAttributes.addFlashAttribute("message", "发布成功!");
		return "redirect:"+"/notice";
	}
	
	@RequestMapping(value = "delete")
	public String delete(Notice notice, RedirectAttributes redirectAttributes) {
		noticeService.delete(notice);
		return "redirect:"+"/notice";
	}

}