package com.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.demo.utils.Global;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.demo.utils.StringUtils;
import com.demo.utils.UserUtils;
import com.demo.bean.Active;
import com.demo.bean.Choice;
import com.demo.bean.Course;
import com.demo.bean.Reply;
import com.demo.bean.Result;
import com.demo.bean.User;
import com.demo.service.ActiveService;
import com.demo.service.ChoiceService;
import com.demo.service.CourseService;
import com.demo.service.ReplyService;
import com.demo.service.ResultService;
import com.demo.service.UserService;

/**
 * 互动管理SController
 */
@Controller
@RequestMapping(value = "/active")
public class ActiveController{

	@Autowired
	private ActiveService activeService;
	@Autowired
	UserService userService;
	@Autowired
	private ReplyService replyService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ChoiceService choiceService;
	@Autowired
	private ResultService resultService;
	
	@ModelAttribute
	public Active get(@RequestParam(required=false) String id) {
		Active entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = activeService.get(id);
		}else{
			entity = new Active();
		}
		return entity;
	}
	
	@RequestMapping(value = {"list", ""})
	public String list(Active active,Result res, HttpServletRequest request, RedirectAttributes redirectAttributes,HttpServletResponse response, Model model) {
		User user = UserUtils.getCurrentUser(request);
		model.addAttribute("user",user);
		PageInfo<Active> page = null;
		if(user.getRoleId().equals("3")) {//教师
			active.setUndefine(user.getId());
			page = activeService.findPage(active);	
		}else if(user.getRoleId().equals("4")) {//学生
			List<Result> allList = resultService.findList(res);
			for(int i=0;i<allList.size();i++) {
				List<Result> dataLists = resultService.dataLists(user.getId(), allList.get(i).getCid());
				if(dataLists.size()>0) {
					active.setUndefine(dataLists.get(0).getUndefine());
					page = activeService.findPage(active);
				}else {
					active.setUndefine(user.getId());
					page = activeService.findPage(active);
				}
			}			
		}
		else {  // 其他
			page = activeService.findPage(active);	
		}
		model.addAttribute("pageInfo", page);
		return "/demo/activeList";
	}
	
	@RequestMapping(value = "infolook")
	public String form(Active active, Model model, HttpServletRequest request) {
		User curUser = UserUtils.getCurrentUser(request);
		model.addAttribute("curUser", curUser);
		//获取评论内容
		Reply reply = new Reply();
		reply.setAid(active.getId());
		List<Reply> commentList = replyService.findList(reply);
		model.addAttribute("commentList", commentList);
		model.addAttribute("active", active);
		return "/demo/infoLook";
	}

	@RequestMapping(value = "form")
	public String form(Active active, User user,Course course, Model model,HttpServletRequest request) {
		User curUser = UserUtils.getCurrentUser(request);
		List<User> users = userService.findTeacherList(user);
		model.addAttribute("users",users);
		List<Course> page = null;
		  if(curUser.getRoleId().equals("3")) {
			  course.setCteacher(curUser.getId());
			  page = courseService.findList(course);
		  }else {
			  page = courseService.findList(course); 
			  }
		  model.addAttribute("page", page);
		model.addAttribute("active", active);
		return "/demo/activeForm";
	}

	@RequestMapping(value = "save")
	public String save(Active active, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
		User user = UserUtils.getCurrentUser(request);
		/* 根据课程ID查找，是否有该课程的互动信息 */
		List<Active> list= activeService.getByCid(active.getCid());
		if(list.size()>0) {
			redirectAttributes.addFlashAttribute("message", "请课程已有互动信息，请重新添加");
		}else {
			active.setStime(new Date());
			active.setUndefine(user.getId());
			activeService.save(active);
		}		
		return "redirect:"+"/active";
	}
	
	@RequestMapping(value = "delete")
	public String delete(Active active, RedirectAttributes redirectAttributes) {
		activeService.delete(active);
		redirectAttributes.addFlashAttribute("message", "删除成功");
		return "redirect:"+"/active";
	}

}