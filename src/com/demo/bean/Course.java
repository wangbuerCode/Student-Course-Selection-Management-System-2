package com.demo.bean;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;
import com.demo.utils.excel.annotation.ExcelField;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 课程Entity
 */
public class Course extends DataEntity<Course> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String cname;		// 课程名称
	private String cteacher;		// 任课教师
	private String classhour;		// 课时
	private Date stime;      // 开始时间
	private Date etime;      // 结束时间
	
	
	private String stimeStr;
	private String etimeStr;
	private String username;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getStimeStr() {
		return stimeStr;
	}

	public void setStimeStr(String stimeStr) {
		this.stimeStr = stimeStr;
	}

	public String getEtimeStr() {
		return etimeStr;
	}

	public void setEtimeStr(String etimeStr) {
		this.etimeStr = etimeStr;
	}

	public Course() {
		super();
	}

	public Course(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getCteacher() {
		return cteacher;
	}

	public void setCteacher(String cteacher) {
		this.cteacher = cteacher;
	}

	public String getClasshour() {
		return classhour;
	}

	public void setClasshour(String classhour) {
		this.classhour = classhour;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:MM:ss")
	@ExcelField(title="开始时间", align=2, sort=30)
	public Date getStime() {
		return stime;
	}

	public void setStime(Date stime) {
		this.stime = stime;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:MM:ss")
	@ExcelField(title="结束时间", align=2, sort=30)
	public Date getEtime() {
		return etime;
	}

	public void setEtime(Date etime) {
		this.etime = etime;
	}

	
}