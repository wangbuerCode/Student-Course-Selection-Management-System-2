package com.demo.bean;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;

/**
 * 系统公告Entity
 */
public class Notice extends DataEntity<Notice> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String title;		// 标题
	private String content;		// 描述
	private String type;		// 类型
	private String status;		// 是否显示
	private Date addTime;      // 添加时间
	
	public Notice() {
		super();
	}

	public Notice(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	

	
}