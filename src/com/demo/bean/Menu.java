package com.demo.bean;

import org.hibernate.validator.constraints.Length;
import java.util.Date;

import com.demo.common.DataEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

/**
 * 系统菜单Entity
 */
public class Menu extends DataEntity<Menu> {
	
	private String id; //编号
	private String name;		// 菜单名称
	private String url;		// 访问地址
	private String fjId;		// 父级编号
	private String fjIds;		// 所有父级编号
	private Integer sort;		// 排序
	
	private String fjName;
	
	public Menu() {
		super();
	}
	
	public Menu(String name, String url, String fjId, String fjIds, Integer sort) {
		super();
		this.name = name;
		this.url = url;
		this.fjId = fjId;
		this.fjIds = fjIds;
		this.sort = sort;
	}



	public Menu(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	@Length(min=0, max=64, message="菜单名称长度必须介于 0 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="访问地址长度必须介于 0 和 255 之间")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getFjId() {
		return fjId;
	}

	public void setFjId(String fjId) {
		this.fjId = fjId;
	}
	
	@Length(min=0, max=255, message="所有父级编号长度必须介于 0 和 255 之间")
	public String getFjIds() {
		return fjIds;
	}

	public void setFjIds(String fjIds) {
		this.fjIds = fjIds;
	}
	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getFjName() {
		return fjName;
	}

	public void setFjName(String fjName) {
		this.fjName = fjName;
	}
	
	
	
}