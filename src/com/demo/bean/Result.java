package com.demo.bean;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;

/**
 * 成绩Entity
 */
public class Result extends DataEntity<Result> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String sid;		// 学生
	private String cid;		// 课程
	private Integer result;		// 成绩
	private String undefine;		// 教师
	
	private String stuname;	
	private String couname;	
	
	public Result() {
		super();
	}

	public Result(String id){
		this.id = id;
	}

	
	
	public String getStuname() {
		return stuname;
	}

	public void setStuname(String stuname) {
		this.stuname = stuname;
	}

	public String getCouname() {
		return couname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getUndefine() {
		return undefine;
	}

	public void setUndefine(String undefine) {
		this.undefine = undefine;
	}

	
	
}