package com.demo.bean;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 互动管理Entity
 */
public class Active extends DataEntity<Active> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String title;		// 标题
	private String content;		// 描述
	private String undefine;	// 教师
	private String cid;			// 课程
	private Date stime;      // 添加时间
	
	private String username;
	private String couname;
	
	public Active() {
		super();
	}

	public Active(String id){
		this.id = id;
	}
	

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getCouname() {
		return couname;
	}

	public void setCouname(String couname) {
		this.couname = couname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUndefine() {
		return undefine;
	}

	public void setUndefine(String undefine) {
		this.undefine = undefine;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:MM:ss")
	public Date getStime() {
		return stime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:MM:ss")
	public void setStime(Date stime) {
		this.stime = stime;
	}


	

	
}