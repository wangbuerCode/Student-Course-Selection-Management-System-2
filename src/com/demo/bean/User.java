package com.demo.bean;

import org.hibernate.validator.constraints.Length;
import java.util.Date;

import com.demo.common.DataEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.demo.utils.excel.annotation.ExcelField;

import javax.validation.constraints.NotNull;

/**
 * 系统用户Entity
 */
public class User extends DataEntity<User> {
	
	private String id;
	private String userNo;		// 用户编号
	private String username;		// 用户名
	private String password;		// 密码
	private Date birth;		// 生日
	private String trueName;		// 真实姓名
	private String email;		// 邮箱
	private String sex;		// 性别
	private String address;		// 家庭住址
	private String phone;		// 手机
	private String age;		// 年龄
	private String isBlock;		// 是否锁定
	private String roleId;		// 角色编号
	private String deptId;		// 部门编号
	private String url;
	
	private String birthstr;
	private String roleName;
	
	public String getBirthstr() {
		return birthstr;
	}

	public void setBirthstr(String birthstr) {
		this.birthstr = birthstr;
	}

	public User() {
	}
	
	public User(String id) {
		this.id = id;
	}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	@Length(min=0, max=64, message="用户编号长度必须介于 0 和 64 之间")
	@ExcelField(title="用户编号", align=2, sort=10)
	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	
	@Length(min=0, max=255, message="用户名长度必须介于 0 和 255 之间")
	@ExcelField(title="用户名", align=2, sort=20)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Length(min=0, max=255, message="密码长度必须介于 0 和 255 之间")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="生日不能为空")
	@ExcelField(title="出生日期", align=2, sort=30)
	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
	@Length(min=0, max=255, message="真实姓名长度必须介于 0 和 255 之间")
	@ExcelField(title="真实姓名", align=2, sort=40)
	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	
	@Length(min=0, max=255, message="邮箱长度必须介于 0 和 255 之间")
	@ExcelField(title="邮箱", align=2, sort=50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Length(min=0, max=1, message="性别长度必须介于 0 和 1 之间")
	@ExcelField(title="性别", align=2, sort=60)
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@Length(min=0, max=255, message="家庭住址长度必须介于 0 和 255 之间")
	@ExcelField(title="家庭住址", align=2, sort=70)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=64, message="手机长度必须介于 0 和 64 之间")
	@ExcelField(title="手机号码", align=2, sort=80)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=11, message="年龄长度必须介于 0 和 11 之间")
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
	@Length(min=0, max=11, message="是否锁定长度必须介于 0 和 11 之间")
	public String getIsBlock() {
		return isBlock;
	}

	public void setIsBlock(String isBlock) {
		this.isBlock = isBlock;
	}
	
	@Length(min=1, max=11, message="角色编号长度必须介于 1 和 11 之间")
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	@Length(min=1, max=11, message="部门编号长度必须介于 1 和 11 之间")
	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}