package com.demo.bean;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;
import com.demo.utils.excel.annotation.ExcelField;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Entity
 */
public class Choice extends DataEntity<Choice> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String cid;		// 课程
	private String sid;		// 学生
	private String tid;		// 教师
	
	public Choice() {
		super();
	}

	public Choice(String id){
		this.id = id;
	}
	
	private String teaname;
	private String stuname;
	private String couname;
	
	
	public String getTeaname() {
		return teaname;
	}

	public void setTeaname(String teaname) {
		this.teaname = teaname;
	}

	public String getStuname() {
		return stuname;
	}

	public void setStuname(String stuname) {
		this.stuname = stuname;
	}

	public String getCouname() {
		return couname;
	}

	public void setCouname(String couname) {
		this.couname = couname;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	
	
}