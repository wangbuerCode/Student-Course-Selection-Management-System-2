package com.demo.bean;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;

/**
 * 系统角色Entity
 */
public class Role extends DataEntity<Role> {
	
	private String id; //编号
	private String name;		// 角色名称
	
	public Role() {
		super();
	}

	public Role(String id){
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Length(min=0, max=255, message="角色名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}