package com.demo.bean;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.demo.common.DataEntity;

/**
 * 回复Entity
 */
public class Reply extends DataEntity<Reply> {
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String tid;		// 教师id
	private String sid;		// 学生id
	private String aid;		// 互动id
	private String content;		// 内容
	private Date atime;      // 发布时间
	private Date rtime;      // 回复时间
	private String undefine; // 课程id
	
	private String teaname;
	private String stuname;
	private String coutitle;
	
	public Reply() {
		super();
	}

	public Reply(String id){
		this.id = id;
	}

	
	public String getTeaname() {
		return teaname;
	}

	public void setTeaname(String teaname) {
		this.teaname = teaname;
	}

	public String getStuname() {
		return stuname;
	}

	public void setStuname(String stuname) {
		this.stuname = stuname;
	}

	public String getCoutitle() {
		return coutitle;
	}

	public void setCoutitle(String coutitle) {
		this.coutitle = coutitle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAtime() {
		return atime;
	}

	public void setAtime(Date atime) {
		this.atime = atime;
	}

	public Date getRtime() {
		return rtime;
	}

	public void setRtime(Date rtime) {
		this.rtime = rtime;
	}

	public String getUndefine() {
		return undefine;
	}

	public void setUndefine(String undefine) {
		this.undefine = undefine;
	}
	

}