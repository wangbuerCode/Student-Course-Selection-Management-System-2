package com.demo.utils;

import javax.servlet.http.HttpServletRequest;

import com.demo.bean.User;

public class UserUtils {

	//返回当前登录的用户
	public static User getCurrentUser(HttpServletRequest request){
		User user = null;
		Object attribute = request.getSession().getAttribute("currentUser");
		if (attribute!=null && attribute instanceof User) {
			user = (User) attribute;
		}
		return user;
	}
}
