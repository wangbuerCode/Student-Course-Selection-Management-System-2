package com.demo.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.demo.utils.Global;

public class TestGlobal {
	
	
	
	
	public static void main(String[] args) {
		String jdbcUrl = Global.getConfig("jdbc.jdbcUrl");
		System.err.println(jdbcUrl);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    try
	    {
	      Date d1 = df.parse("2004-03-26 13:31:40");
	      Date d2 = df.parse("2004-03-26 13:35:45");
	      Integer diff = (int) (d1.getTime() - d2.getTime());//这样得到的差值是微秒级别
	      Integer days = diff / (1000 * 60 * 60 * 24);
	      Integer hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);
	      Integer minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60);
	      Integer s = (diff / 1000 - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60);
	      System.out.println(""+days+"天"+hours+"小时"+minutes+"分"+s+"秒");
	    }catch (Exception e)
	    {
	    }
		
	}
	
	
}
