package com.demo.dao;

import com.demo.common.CrudDao;

import java.util.List;

import com.demo.bean.Active;
import com.demo.bean.Notice;

/**
 * 互动管理DAO接口
 */
public interface ActiveDao extends CrudDao<Active> {
	
	 List<Active> getByCid (String cid);
}