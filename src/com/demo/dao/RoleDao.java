/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.dao;

import com.demo.bean.Role;
import com.demo.common.CrudDao;

/**
 * 系统角色DAO接口
 * @author s
 * @version 2017-05-06
 */
public interface RoleDao extends CrudDao<Role> {
	
}