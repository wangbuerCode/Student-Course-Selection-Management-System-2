/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.dao;

import java.util.List;

import com.demo.bean.Menu;
import com.demo.common.CrudDao;

/**
 * 系统菜单DAO接口
 * @author s
 * @version 2017-05-05
 */
public interface MenuDao extends CrudDao<Menu> {
	//根据父级编号查询菜单；0查询所有父级菜单
	public List<Menu> getByFjId(String fjId);
}