/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.dao;

import java.util.List;

import com.demo.bean.Authority;
import com.demo.common.CrudDao;

/**
 * 系统用户DAO接口
 * @author sys
 * @version 2017-05-05
 */
public interface AuthorityDao extends CrudDao<Authority> {
	public List<Authority> getByRoleId(String roleId);
	
	public List<String> getMenuIds(String roleId);
}