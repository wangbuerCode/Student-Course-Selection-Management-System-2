package com.demo.dao;

import com.demo.common.CrudDao;
import com.demo.bean.Notice;

/**
 * 系统公告DAO接口
 */
public interface NoticeDao extends CrudDao<Notice> {
	
}