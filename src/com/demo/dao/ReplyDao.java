package com.demo.dao;

import com.demo.common.CrudDao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.demo.bean.Active;
import com.demo.bean.Notice;
import com.demo.bean.Reply;
import com.demo.bean.Result;

/**
 * 回复DAO接口
 */
public interface ReplyDao extends CrudDao<Reply> {
	List<Reply> dataList(@Param("sid") String gid, @Param("cid")String bid);// 学生和课程
	
}