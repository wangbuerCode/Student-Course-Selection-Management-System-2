/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.demo.dao;

import com.demo.common.CrudDao;
import com.demo.bean.SysDict;

/**
 * 字典管理DAO接口
 * @author demo_cxy
 *
 */
public interface SysDictDao extends CrudDao<SysDict> {
	
}