package com.demo.dao;

import com.demo.common.CrudDao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.demo.bean.Choice;
import com.demo.bean.Notice;
import com.demo.bean.Result;

public interface ResultDao extends CrudDao<Result> {
	
	
	List<Result> dataList(@Param("sid") String gid, @Param("cid")String bid);// 学生和课程
	
	Result del(@Param("sid") String gid, @Param("cid")String bid);// 删除
	
}