package com.demo.dao;

import java.util.List;

import com.demo.bean.User;
import com.demo.common.CrudDao;

/**
 * 系统用户DAO接口
 */
public interface UserDao extends CrudDao<User> {
	public int block(User user);
	
	public User login(User user);
	
	
}