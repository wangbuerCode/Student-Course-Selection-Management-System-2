package com.demo.dao;

import com.demo.common.CrudDao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.demo.bean.Choice;
import com.demo.bean.Course;
import com.demo.bean.Notice;

/**
 * DAO接口
 */
public interface ChoiceDao extends CrudDao<Choice> {
	
	List<Choice> dataList(@Param("sid") String gid, @Param("cid")String bid);// 学生和课程
	List<Choice> dataLists(@Param("tid") String tids, @Param("cid")String cid); // 教师和课程
	
	List<Choice> dataStuList(@Param("sid") String gid);// 学生和课程
}