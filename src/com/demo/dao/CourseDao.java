package com.demo.dao;

import com.demo.common.CrudDao;
import com.demo.bean.Course;
import com.demo.bean.Notice;

/**
 * 课程DAO接口
 */
public interface CourseDao extends CrudDao<Course> {
	
}