<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>测试管理</title>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<script src="${ctxStatic}/echarts.js" type="text/javascript"
	charset="utf-8"></script>
<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
	
 <div class="panel panel-default">
 
	<div>
	  <%-- <img alt="" src="${adminPath1}/111.jpeg" style="width: 1524px;height: 300px;" > --%>
	  <img alt="" src="${adminPath1}/111.jpeg" style="width: 1348px;height: 300px;" >
	</div>
 
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="${adminPath}/notice/">系统公告列表</a></li>
		</ul>
	</div>
	${message}
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>序号</th>
				<th>标题</th>
				<th>类型</th>
				<th>发布时间</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pageInfo.list}" var="notice"  varStatus="index_no">
			<tr>
				<td>${index_no.index+1+(pageInfo.pageNum-1)*pageInfo.pageSize }</td>
				<td><a href='${adminPath }/notice/look?id=${notice.id}'>${notice.title}</a>
				</td>
				<td>
					${notice.type=="1"?"教学内容":"系统公告"}
				</td>
				<td>
					<fmt:formatDate value="${notice.addTime}" pattern="yyyy-MM-dd"/>
				</td>
				
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<!-- 分页条 -->
		<div class="panel-footer">
			<!-- 显示分页信息 -->
		<div class="row">
			<!--分页文字信息  -->
			<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
				页,总 ${pageInfo.total } 条记录</div>
			<!-- 分页条信息 -->
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>


					<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
						<c:if test="${page_Num == pageInfo.pageNum }">
							<li class="active"><a href="#">${page_Num }</a></li>
						</c:if>
						<c:if test="${page_Num != pageInfo.pageNum }">
							<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
						</c:if>

					</c:forEach>
					<c:if test="${pageInfo.hasNextPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
 
</body>
</html>
