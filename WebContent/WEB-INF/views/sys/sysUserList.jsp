<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<%@include file="/WEB-INF/views/include/head.jsp" %>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	function page(n){
		$("#pageNo").val(n);
		$("#searchForm").submit();
       	return false;
       }
	
	//点击导入按钮弹出模态框。（此方法不可以用，原因不详！）
	function add(){
		//发送ajax获得所有菜单
		getDepts("#empAddModal select");
		$("#empAddModal").modal();
	}
</script>
</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li class="active"><a
					href="${adminPath }/system/user/list">用户列表</a></li>
				<li><a href="${adminPath }/system/user/form">用户添加</a></li>
			</ul>
		</div>
		<div class="panel-body">
			<form:form id="searchForm" modelAttribute="user" action="${adminPath}/system/user/list" method="post" class="form-horizontal">
				<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNum}"/>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">用户名</label>
	   				<div class="col-sm-2">
	   					<form:input path="username" htmlEscape="false" class="form-control input-sm" placeholder="用户名称"/>
	   				</div>
	   				<label for="firstname" class="col-sm-1 control-label">真实姓名</label>
	   				<div class="col-sm-2">
	   					<form:input path="trueName" htmlEscape="false" class="form-control input-sm required digits"/>
	   				</div>
					
					<button class="btn btn-success" type="submit">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						查询
					</button>
					
				</div>
			</form:form>
		</div>
		<c:if test="${!empty message }">
			<div id="myAlert" class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong>成功！</strong>${message }
			</div>
		</c:if>
		<!-- Table -->
		<table id="contentTable" class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>序号</th>
					<!-- <th>用户编号</th> -->
					<th>用户账号</th>
					<th>生日</th>
					<th>真实姓名</th>
					<th>邮箱</th>
					<th>性别</th>
					<th>手机</th>
					<th>角色</th>
					<th>是否锁定</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pageInfo.list }" var="user" varStatus="index_no">
					<tr>
						<td>${index_no.index+1+(pageInfo.pageNum-1)*pageInfo.pageSize }</td>
						<%-- <td>${user.userNo }</td> --%>
						<td>${user.username }</td>
						<td><fmt:formatDate value="${user.birth}" pattern="yyyy-MM-dd"/></td>
						<td>${user.trueName }</td>
						<td>${user.email }</td>
						<td>${user.sex=="0"?"男":"女" }</td>
						<td>${user.phone }</td>
						<td>${user.roleName }</td>
						<td>${user.isBlock=="0"?"可用":"锁定" }</td>
						<td>
							<a href="${adminPath }/system/user/returnPassword?id=${user.id}">
									<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
										重置密码
									</button>
								</a>
							<c:if test="${user.isBlock=='0' }">
								<a href="${adminPath }/system/user/block?id=${user.id}&isBlock=1">
									<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
										锁定
									</button>
								</a>
							</c:if> <c:if test="${user.isBlock=='1'}">
								<a href="${adminPath }/system/user/block?id=${user.id}&isBlock=0">
									<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										解锁
									</button>
								</a>
							</c:if>
							<a href="${adminPath }/system/user/form?id=${user.id}">
								<button class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									编辑
								</button>
							</a>
							<a href="${adminPath }/system/user/del?id=${user.id}" onclick="return confirm('确认要删除该用户吗？', this.href)">
								<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									删除
								</button>
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<!-- 分页条 -->
		<div class="panel-footer">
			<!-- 显示分页信息 -->
		<div class="row">
			<!--分页文字信息  -->
			<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
				页,总 ${pageInfo.total } 条记录</div>
			<!-- 分页条信息 -->
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>


					<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
						<c:if test="${page_Num == pageInfo.pageNum }">
							<li class="active"><a href="#">${page_Num }</a></li>
						</c:if>
						<c:if test="${page_Num != pageInfo.pageNum }">
							<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
						</c:if>

					</c:forEach>
					<c:if test="${pageInfo.hasNextPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
	
	<!-- 导入模态框 -->
	<div class="modal fade" id="empAddModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<form class="form-horizontal" action="${adminPath}/system/user/import" enctype="multipart/form-data"
				method="post">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">导入用户</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">选择文件</label>
							<div class="col-sm-10">
								<input type="file" name="file"> <span
									class="help-block"></span>
									<a href="${adminPath }/system/user/import/template">下载模板</a>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">导入</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>