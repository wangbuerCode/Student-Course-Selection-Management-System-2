<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%
	pageContext.setAttribute("adminPath", request.getContextPath());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="${adminPath }/static/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="${adminPath }/static/js/jquery.js"></script>

<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('ul').slideUp();
		if($ul.is(':visible')){
			$(this).next('ul').slideUp();
		}else{
			$(this).next('ul').slideDown();
		}
	});
})	
</script>


</head>

<body style="background:#f0f9fd;">
	<div class="lefttop"><span></span>菜单列表</div>
    
    <dl class="leftmenu">
        
        <c:forEach var="list" items="${list }">
        	<dd>
        	 <c:forEach var="authority1" items="${authorityList }">
        	 	<c:if test="${list.parentMenu.id==authority1.menuId }">
				    <div class="title">
				    <span><img src="${adminPath }/static/images/leftico01.png" /></span>${list.parentMenu.name }
				    </div>
				    <ul class="menuson">
			    	<c:forEach items="${list.childMenus }" var="menu">
			    		<c:forEach var="authority2" items="${authorityList }">
			    			<c:if test="${menu.id==authority2.menuId }">
						        <li><cite></cite><a href="${adminPath }${menu.url }" target="rightFrame">${menu.name }</a><i></i></li>
			    			</c:if>
			    		</c:forEach>
			    	</c:forEach>
			        </ul> 
        	 	</c:if>
        	 </c:forEach>
		    	   
			 </dd>
        </c:forEach>
    </dl>
    
</body>
</html>