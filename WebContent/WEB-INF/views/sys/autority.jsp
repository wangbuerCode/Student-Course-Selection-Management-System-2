<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>

<script type="text/javascript" src="${adminPath }/static/js/jquery-1.12.4.min.js"></script>
<link href="${adminPath }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
<script src="${adminPath }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	
<script type="text/javascript">
	function save(){
		var roleId = document.getElementById("role_id").value;
		var objs = document.getElementsByName("menu_checkbox");
		var str="";
		for(var i=0;i<objs.length;i++){
			if(objs[i].checked){
				str+=objs[i].value+",";
			}
		}
		if(str==""){
			alert("必须勾选一个菜单！");
		}else{
			$.ajax({
				url:"${adminPath}/system/role/saveAutory?roleId="+roleId+"&str="+str,
				type:"GET",
				success:function(result){
					alert("更新权限成功！");
				}
			});
		}
	}
</script>
</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li><a href="${adminPath }/system/role/list">角色列表</a></li>
				<li class="active"><a href="javascript:void(0);">权限分配</a></li>
			</ul>
		</div>
		<div class="panel-body">
			<input type="hidden" id="role_id" value="${roleId }">
			<c:forEach var="list" items="${list }">
		        	<ul>
		        		<li><input type="checkbox" name="menu_checkbox" value="${list.parentMenu.id }" <c:if test="${fn:contains(roldIds, list.parentMenu.id) }">checked="checked"</c:if> > ${list.parentMenu.name }</li>
		        		<li>
		        			<ul>
		        				<c:forEach items="${list.childMenus }" var="menu">
		        					<li><input type="checkbox" name="menu_checkbox" value="${menu.id }" <c:if test="${fn:contains(roldIds, list.parentMenu.id) }">checked="checked" </c:if> >${menu.name }</li>
		        				</c:forEach>
		        			</ul>
		        		</li>
		        	</ul>
		    </c:forEach>
		    <button onclick="save();" class="btn btn-primary">保存</button>
		</div>
	</div>
</body>
</html>