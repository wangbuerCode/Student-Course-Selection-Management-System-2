<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>员工列表</title>
<%@include file="/WEB-INF/views/include/head.jsp" %>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	function page(n){
		$("#pageNo").val(n);
		$("#searchForm").submit();
       	return false;
       }
</script>
<script type="text/javascript">
	//点击新增按钮弹出模态框。（此方法不可以用，原因不详！）
	function add(){
		//发送ajax获得所有菜单
		getDepts("#empAddModal select");
		$("#empAddModal").modal();
	}
	//页面加载时查询数据获得所有一级菜单
	$(document).ready(function() {
		//发送ajax获得所有菜单
	});
	
	
	
	//查出所有一级菜单并显示在下拉列表中
	function getDepts(ele){
		//清空之前下拉列表的值
		$(ele).empty();
		$.ajax({
			url:"${adminPath}/system/role/roleJson",
			type:"GET",
			success:function(result){
				var optionEle = $("<option></option>").append("请选择").attr("value",0);
				optionEle.appendTo(ele);
				$.each(result.extend.depts,function(){
					optionEle = $("<option></option>").append(this.name).attr("value",this.id);
					optionEle.appendTo(ele);
				});
			}
		});
		
	}

	//点击新增按钮弹出模态框。
	function edit(id){
		//发送ajax获得所有菜单
		getDeptsToUpdate("#empUpdateModal select",id);
		$("#empUpdateModal").modal({
			backdrop:"static"
		});
	}
	//查出所有一级菜单并显示在下拉列表中
	function getDeptsToUpdate(ele,id){
		//清空之前下拉列表的值
		$(ele).empty();
		$.ajax({
			url:"${adminPath}/system/role/roleJson?id="+id,
			type:"GET",
			success:function(result){
				$("#roleId").attr("value",result.extend.role.id);
				$("#roleName").attr("value",result.extend.role.name);
			}
		});
		
	}

</script>

<SCRIPT type="text/javascript">
		 
		//设置权限
		function setAutority(id){
			//发送ajax获得所有菜单
			/* getDepts("#empAuthorityModal select"); */
			$.ajax({
				url:"${adminPath}/system/menu/menusJson?id="+id,
				type:"GET",
				success:function(result){
					zNodes = result.extend.list; 
					var maps = eval(zNodes);
					for(var i=0;i<maps.length;i++){
						alert(maps[i].name);
					}
					setDates(); 
				}
			});
			$("#empAuthorityModal").modal();
		}
		
	</SCRIPT>

</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li class="active"><a href="${adminPath }/system/role/list">角色列表</a></li>
			</ul>
		</div>
		<div class="panel-body">
			<form:form id="searchForm" modelAttribute="role" action="${adminPath}/system/role/list" method="post" class="form-horizontal">
				<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNum}"/>
				<div class="form-group">
					<label for="firstname" class="col-sm-1 control-label">角色名称</label>
					<div class="col-xs-2">
						<form:input path="name" htmlEscape="false" class="form-control input-sm" placeholder="角色名称"/>
					</div>
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#empAddModal">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						添加
					</button>
					<button class="btn btn-success" type="submit">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
						查询
					</button>
				</div>
			</form:form>
		</div>

		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>序号</th>
					<th>角色编号</th>
					<th>角色名</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pageInfo.list }" var="role" varStatus="index_no">
					<tr>
						<td>${index_no.index+1+(pageInfo.pageNum-1)*5 }</td>
						<td>${role.id }</td>
						<td>${role.name }</td>
						<td><a
							href="${adminPath}/system/role/setAutory?roleId=${role.id }">
								<button class="btn btn-success btn-xs">
									<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
									分配权限
								</button>
						</a>
								<button class="btn btn-primary btn-xs"
									onclick="edit('${role.id}');">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									编辑
								</button>
								<c:if test="${role.id!=2 }">
								 <a href="${adminPath}/system/role/del?id=${role.id}">
									<button class="btn btn-danger btn-xs">
										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										删除
									</button>
								</a>
								</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="panel-footer">
				<!-- 显示分页信息 -->
			<div class="row">
				<!--分页文字信息  -->
				<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
					页,总 ${pageInfo.total } 条记录</div>
				<!-- 分页条信息 -->
				<div class="col-md-6">
					<nav aria-label="Page navigation">
					<ul class="pagination">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
						<c:if test="${pageInfo.hasPreviousPage }">
							<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
								aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
							</a></li>
						</c:if>
	
	
						<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
							<c:if test="${page_Num == pageInfo.pageNum }">
								<li class="active"><a href="#">${page_Num }</a></li>
							</c:if>
							<c:if test="${page_Num != pageInfo.pageNum }">
								<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
							</c:if>
	
						</c:forEach>
						<c:if test="${pageInfo.hasNextPage }">
							<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
								aria-label="Next"> <span aria-hidden="true">&raquo;</span>
							</a></li>
						</c:if>
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal 新增弹出框-->
	<div class="modal fade" id="empAddModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<form class="form-horizontal" action="${adminPath}/system/role/save"
				method="post">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">添加角色</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">角色名</label>
							<div class="col-sm-10">
								<input type="text" name="name" class="form-control"
									id="menuName_add" placeholder="empName"> <span
									class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">保存</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Modal 更新弹出框-->
	<div class="modal fade" id="empUpdateModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<form class="form-horizontal" action="${adminPath}/system/role/update"
				method="post">
				<input type="hidden" name="id" id="roleId">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">更新角色</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-2 control-label">角色名称</label>
							<div class="col-sm-10">
								<input type="text" name="name" class="form-control"
									id="roleName" placeholder="empName"> <span
									class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">保存</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>