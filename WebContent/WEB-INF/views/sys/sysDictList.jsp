<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>字典管理管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n){
			$("#pageNo").val(n);
			$("#searchForm").submit();
	       	return false;
	       }
	</script>
</head>
<body>
<div class="panel panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="${adminPath}/demo/sysDict/">字典管理列表</a></li>
			<li><a href="${adminPath}/demo/sysDict/form">字典管理添加</a></li>
		</ul>
	</div>
	<div class="panel-body">
	<form:form id="searchForm" modelAttribute="sysDict" action="${adminPath}/demo/sysDict/" method="post" class="form-horizontal">
		<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNum}"/>
		<div class="form-group">
			<label for="firstname" class="col-sm-1 control-label">标签名：</label>
				<div class="col-sm-2">
   					<form:input path="label" htmlEscape="false" class="form-control input-sm" placeholder="标签名"/>
   				</div>
			<label for="firstname" class="col-sm-1 control-label">类型：</label>
				<div class="col-sm-2">
   					<form:input path="type" htmlEscape="false" class="form-control input-sm" placeholder="类型"/>
   				</div>
			<label for="firstname" class="col-sm-1 control-label">描述：</label>
				<div class="col-sm-2">
   					<form:input path="description" htmlEscape="false" class="form-control input-sm" placeholder="描述"/>
   				</div>
			<button class="btn btn-success" type="submit">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				查询
			</button>
		</div>
	</form:form>
	</div>
	${message}
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>序号</th>
				<th>数据值</th>
				<th>标签名</th>
				<th>类型</th>
				<th>描述</th>
				<th>排序（升序）</th>
				<th>父级编号</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pageInfo.list}" var="sysDict"  varStatus="index_no">
			<tr>
				<td>${index_no.index+1+(pageInfo.pageNum-1)*5 }</td>
				<td><a href="${adminPath}/demo/sysDict/form?id=${sysDict.id}">
					${sysDict.value}
				</a></td>
				<td>
					${sysDict.label}
				</td>
				<td>
					${sysDict.type}
				</td>
				<td>
					${sysDict.description}
				</td>
				<td>
					${sysDict.sort}
				</td>
				<td>
					${sysDict.parent.id}
				</td>
				<td>
					<fmt:formatDate value="${sysDict.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${sysDict.remarks}
				</td>
				<td>
    				<a href="${adminPath}/demo/sysDict/form?id=${sysDict.id}">
    					<button class="btn btn-primary btn-sm" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							编辑
						</button>
    				</a>
					<a href="${adminPath}/demo/sysDict/delete?id=${sysDict.id}">
						<button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							删除
						</button>
					</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<!-- 分页条 -->
		<div class="panel-footer">
			<!-- 显示分页信息 -->
		<div class="row">
			<!--分页文字信息  -->
			<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
				页,总 ${pageInfo.total } 条记录</div>
			<!-- 分页条信息 -->
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>


					<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
						<c:if test="${page_Num == pageInfo.pageNum }">
							<li class="active"><a href="#">${page_Num }</a></li>
						</c:if>
						<c:if test="${page_Num != pageInfo.pageNum }">
							<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
						</c:if>

					</c:forEach>
					<c:if test="${pageInfo.hasNextPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
</body>
</html>