<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>字典管理管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
	<div class="panel panel-default">
	<div class="panel-heading">
	<ul class="nav nav-tabs">
		<li><a href="${adminPath}/demo/sysDict/">字典管理列表</a></li>
		<li class="active"><a href="${adminPath}/demo/sysDict/form?id=${sysDict.id}">字典管理新增</a></li>
	</ul>
	</div>
	<div class="panel-body">
	<form:form id="inputForm" modelAttribute="sysDict" action="${adminPath}/demo/sysDict/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">数据值</label>
   				<div class="col-sm-2">
			
						<form:input path="value" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
						<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">标签名</label>
   				<div class="col-sm-2">
			
						<form:input path="label" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
						<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">类型</label>
   				<div class="col-sm-2">
			
						<form:input path="type" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
						<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">描述</label>
   				<div class="col-sm-2">
			
						<form:input path="description" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
						<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">排序（升序）</label>
   				<div class="col-sm-2">
			
						<form:input path="sort" htmlEscape="false" class="input-xlarge required"/>
						<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">父级编号</label>
   				<div class="col-sm-2">
			
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">备注信息</label>
   				<div class="col-sm-2">
			
						<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
				</div>
			</div>
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
	</form:form>
	</div>
	</div>
</body>
</html>