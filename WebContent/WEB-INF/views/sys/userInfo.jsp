<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/WEB-INF/views/include/head.jsp" %>
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li class="active"><a href="${adminPath }/system/user/userInfo">个人信息</a></li>
				<li><a href="${adminPath }/system/user/toSetPass">修改密码</a></li>
			</ul>
		</div>
		<div class="panel-body">
		<form:form id="inputForm" modelAttribute="user" action="${adminPath}/system/user/fixInfo" method="post" class="form-horizontal" enctype="multipart/form-data" >
			<form:hidden path="id"/>
			<span style="color: green;">${message }</span>
			
			<%-- <div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">用户编号</label>
	   			<div class="col-sm-2">
	   				<form:input path="userNo" htmlEscape="false" class="input-small required digits"/>
	   			</div>
			</div> --%>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">用户名</label>
   				<div class="col-sm-2">
   					<form:input path="username" htmlEscape="false" class="input-small required digits" disabled="true"/>
   				</div>
			</div>
			<%-- <div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">密码</label>
   				<div class="col-sm-2">
   					<form:input type="password" path="password" htmlEscape="false" class="input-small required digits"/>
   				</div>
			</div> --%>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">生日</label>
   				<div class="col-sm-4">
   					<input name="birthstr" htmlEscape="false" class="Wdate" readonly="readonly"
   					value="<fmt:formatDate value="${user.birth}" pattern="yyyy-MM-dd"/>" onFocus="WdatePicker({lang:'zh-cn',startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">真实姓名</label>
   				<div class="col-sm-2">
   					<form:input path="trueName" htmlEscape="false" class="input-small required digits"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">邮箱</label>
   				<div class="col-sm-2">
   					<form:input path="email" htmlEscape="false" class="input-small required digits"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">性别</label>
   				<div class="col-sm-2">
   					<form:select path="sex" class="input-xlarge ">
   						<form:option value="0" label="保密"/>
						<form:option value="1" label="男"/>
						<form:option value="2" label="女"/>
					</form:select>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">家庭住址</label>
   				<div class="col-sm-4">
   					<form:input path="address" htmlEscape="false" class="input-small required digits"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">手机</label>
   				<div class="col-sm-2">
   					<form:input path="phone" htmlEscape="false" class="input-small required digits"/>
   				</div>
			</div>
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">角色</label>
   				<div class="col-sm-2">
   					<input type="text" value="${user.roleName }" disabled="true"/>
   				</div>
			</div>
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
		</form:form>
		</div>
	</div>
</body>
</html>