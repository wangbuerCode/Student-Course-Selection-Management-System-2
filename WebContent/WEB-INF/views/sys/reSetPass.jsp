<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/WEB-INF/views/include/head.jsp" %>
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li><a href="${adminPath }/system/user/userInfo">个人信息</a></li>
				<li  class="active"><a href="${adminPath }/system/user/toSetPass">修改密码</a></li>
			</ul>
		</div>
		<div class="panel-body">
		
		<form:form id="inputForm" modelAttribute="user" action="${adminPath}/system/user/setPassword" method="post" class="form-horizontal">
			<form:hidden path="id"/>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">旧密码</label>
   				<div class="col-sm-2">
   					<input type="password" name="oldPassword" class="input-small required digits" value="${oldPassword }"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">新密码</label>
   				<div class="col-sm-2">
   					<input type="password" name="new1Password" class="input-small required digits" value="${new1Password }"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">确认密码</label>
   				<div class="col-sm-2">
   					<input type="password" name="new2Password" class="input-small required digits" value="${new2Password }"/>
   				</div>
			</div>
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
			<c:if test="${type == '0' }"><font style="color: red">&nbsp;&nbsp;&nbsp;${message }</font></c:if>
			<c:if test="${type == '1' }"><font style="color: green;">&nbsp;&nbsp;&nbsp;${message }</font></c:if>
			
		</form:form>
		
			<!-- 添加表单 col-sm-12-->
		<%-- <form action="${adminPath}/system/user/save" method="post" class="form-horizontal" role="form">
			<input type="hidden" name="id" id="userId" value="${user.id }">
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">用户编号</label>
	   			<div class="col-sm-2">
					<input type="text" name="userNo" class="form-control" id="userNo_add" value="${user.userNo }">
	   			</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">用户名</label>
   				<div class="col-sm-2">
					<input type="text" name="username" class="form-control" id="menuName_add" value="${user.username }"> 
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">密码</label>
   				<div class="col-sm-2">
					<input type="password" name="password" class="form-control" id="menuUrl_add" value="${user.password }">
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">生日</label>
   				<div class="col-sm-4">
					<input type="text" id="menuSort_add" name="birth" class="Wdate" onFocus="WdatePicker({lang:'zh-cn',startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})" value="${user.birth }"/>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">真实姓名</label>
   				<div class="col-sm-2">
					<input type="text" name="trueName" class="form-control" id="menuSort_add" value="${user.trueName }">
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">邮箱</label>
   				<div class="col-sm-2">
					<input type="text" name="email" class="form-control" id="menuSort_add" value="${user.email }">
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">性别</label>
   				<div class="col-sm-2">
					<select class="form-control" name="sex">
						<option value="0" >保密</option>
						<option value="1" >男</option>
						<option value="2">女</option>
					</select>
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">家庭住址</label>
   				<div class="col-sm-4">
					<input type="text" name="address" class="form-control" id="menuSort_add" value="${user.address }">
   				</div>
			</div>
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">手机</label>
   				<div class="col-sm-2">
					<input type="text" name="phone" class="form-control" id="menuSort_add" value="${user.phone }">
   				</div>
			</div>
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">角色</label>
   				<div class="col-sm-2">
					<select class="form-control" name="roleId">
						<c:forEach items="${roles }" var="role">
							<option value="${role.id }">${role.name }</option>
						</c:forEach>
					</select>
   				</div>
			</div>
			<div class="form-group" id="deptSelect">
				<label for="firstname" class="col-sm-1 control-label">部门</label>
   				<div class="col-sm-2">
					<select class="form-control" name="deptId">
						<c:forEach items="${depts }" var="dept">
							<option value="${dept.id }">${dept.name }</option>
						</c:forEach>
					</select>
   				</div>
			</div>
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
		</form>  --%>
		</div>
	</div>
</body>
</html>