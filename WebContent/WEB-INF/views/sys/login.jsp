<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
	pageContext.setAttribute("adminPath", request.getContextPath());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录后台管理系统</title>
<link href="${adminPath }/static/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="${adminPath }/static/js/jquery.js"></script>
<script src="${adminPath }/static/js/cloud.js" type="text/javascript"></script>

<script src="${adminPath }/static/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

<!-- web路径：
不以/开始的相对路径，找资源，以当前资源的路径为基准，经常容易出问题。
以/开始的相对路径，找资源，以服务器的路径为标准(http://localhost:3306)；需要加上项目名
		http://localhost:3306/crud
 -->
<script type="text/javascript"
	src="${adminPath }/static/js/jquery-1.12.4.min.js"></script>
<link
	href="${adminPath }/static/bootstrap-3.3.7-dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="${adminPath }/static/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

<script language="javascript">
	$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
});  
	
	//点击新增按钮弹出模态框。
	function regit(){
		//发送ajax获得所有菜单
		$("#empAddModal").modal({
			backdrop:"static"
		});
	}
</script> 
</head>
<body style="background-color:#1c77ac; background-image:url(${adminPath }/static/images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">
    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>  
	<div class="logintop">    
    <span>欢迎登录后台管理界面平台</span>    
    <ul>

    </ul>    
    </div>
    <div class="loginbody">
	    <form action="${adminPath }/system/user/login" method="post">
		    <span class="systemlogo"></span> 
		    <div class="loginbox">
			    <ul>
				    <li><input name="username" type="text" class="loginuser" value="${username }"/></li>
				    <li><input name="password" type="password" class="loginpwd" value="${password }" /></li>
				    <li><span style="color: red;">${msg }</span></li>
				    <li><input type="submit" class="loginbtn" value="登录"/><label><input name="" type="checkbox" value="" checked="checked" />记住密码</label><label><a href="#" onclick="regit();">没有账号？注册</a></label></li>
			    </ul>
		    </div>
	    </form>
    </div>
   
	
	<!-- Modal 新增弹出框-->
		<div class="modal fade" id="empAddModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<form class="form-horizontal" action="${adminPath}/system/user/regit" method="post">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">用户注册</h4>
						</div>
						<div class="modal-body">
							<!-- <div class="form-group">
								<label class="col-sm-2 control-label">用户编号</label>
								<div class="col-sm-10">
									<input type="text" name="userNo" class="form-control"
										id="userNo_add" placeholder="empName"> <span
										class="help-block"></span>
								</div>
							</div> -->
							<div class="form-group">
								<label class="col-sm-2 control-label">用户账号</label>
								<div class="col-sm-10">
									<input type="text" name="username" class="form-control"
										id="menuName_add" placeholder=""> <span
										class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">密码</label>
								<div class="col-sm-10">
									<input type="password" name="password" class="form-control"
										id="menuUrl_add" placeholder="">
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">生日</label>
								<div class="col-sm-10">
									<input type="text" id="menuSort_add" name="birth" class="Wdate" onFocus="WdatePicker({lang:'zh-cn',startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"/>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">真实姓名</label>
								<div class="col-sm-10">
									<input type="text" name="trueName" class="form-control"
										id="menuSort_add" placeholder="">
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">邮箱</label>
								<div class="col-sm-10">
									<input type="text" name="email" class="form-control"
										id="menuSort_add" placeholder="111@qq.com">
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">性别</label>
								<div class="col-sm-10">
									<!-- <input type="text" name="sex" class="form-control"
										id="menuSort_add" placeholder="1"> -->
									男 <input type="radio" name="sex" value="0">
									女 <input type="radio" name="sex" value="1">
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">家庭住址</label>
								<div class="col-sm-10">
									<input type="text" name="address" class="form-control"
										id="menuSort_add" placeholder="">
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">手机</label>
								<div class="col-sm-10">
									<input type="text" name="phone" class="form-control"
										id="menuSort_add" placeholder="">
									<span class="help-block"></span>
								</div>
							</div>
							<!-- style="display: none;" -->
							<div class="form-group" id="roleSelect" >
								<label class="col-sm-2 control-label">角色</label>
								<div class="col-sm-4">
									<select class="form-control" name="roleId">
										<option value="3">教师</option>
										<option value="4">学生</option>
									</select>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">保存</button>
						</div>
					</div>
				</form>
			</div>
		</div>

</body>
</html>