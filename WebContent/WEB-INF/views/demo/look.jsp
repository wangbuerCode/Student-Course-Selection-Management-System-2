<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<title>公告详情</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<link href="${adminPath }/static/front/CSS/main.css" rel="stylesheet" type="text/css" />
	<script language="javascript">
	</script>
</head>
<body onload="focusOnLogin()">



<link href="CSS/read.css" rel="stylesheet" type="text/css" />

<div id="container">
  


  <div class="main">
    <div class="content1">
      <ul class="classlist">
        <table width="100%" align="center">
          <tr width="100%">
            <td colspan="2" align="center" style="font-size: 16px;">${notice.title }</td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2" >
          	    类型：${notice.type=="1"?"教学公告":"系统公告"}</a>
          	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          	    发布时间：<fmt:formatDate value="${notice.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
          	     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
          	     <a href="${adminPath}/system/user/toIndex">返回</a>
          	    </td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
         
          <tr>
            <td colspan="2" align="center"></td>
          </tr>
          <tr>
            <td colspan="2">
            	${notice.content}
            </td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
        </table>
      </ul>
    </div>
  </div>
</div>
</body>
</html>