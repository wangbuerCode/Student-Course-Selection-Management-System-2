<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>互动管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
	<div class="panel panel-default">
	<div class="panel-heading">
	<ul class="nav nav-tabs">
		<li><a href="${adminPath}/active/">互动列表</a></li>
		<li class="active"><a href="${adminPath}/active/form?id=${active.id}">互动新增</a></li>
	</ul>
	</div>
	<div class="panel-body">
	<form:form id="inputForm" modelAttribute="active" action="${adminPath}/active/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			
			<div class="form-group" id="title" >
				<label for="firstname" class="col-sm-1 control-label">标题</label>
   				<div class="col-sm-2">
						<form:input path="title" htmlEscape="false" maxlength="64" class="form-control "/>
				</div>
			</div>
			<div class="form-group" id="title" >
				<label for="firstname" class="col-sm-1 control-label">关联课程</label>
   				<div class="col-sm-2">
					<form:select path="cid" class="form-control">
						<form:options items="${page}" itemLabel="cname" itemValue="id"/>
					</form:select>
				</div>
			</div>
			<%-- <div class="form-group" id="title" >
				<label for="firstname" class="col-sm-1 control-label">发表教师</label>
   				<div class="col-sm-2">
					<form:select path="undefine" class="form-control">
						<form:options items="${users}" itemLabel="trueName" itemValue="id"/>
					</form:select>
				</div>
			</div> --%>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">公告内容</label>
   				<div class="col-sm-10">
						<form:textarea path="content" htmlEscape="false" rows="25" class="form-control xheditor"/>
				</div>
			</div>
			
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
	</form:form>
	</div>
	</div>
</body>
</html>