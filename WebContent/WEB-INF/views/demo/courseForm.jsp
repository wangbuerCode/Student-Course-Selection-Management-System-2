<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>课程管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
	<div class="panel panel-default">
	<div class="panel-heading">
	<ul class="nav nav-tabs">
		<li><a href="${adminPath}/course/">课程列表</a></li>
		<li class="active"><a href="${adminPath}/course/form?id=${course.id}">课程新增</a></li>
	</ul>
	</div>
	<div class="panel-body">
	<form:form id="inputForm" modelAttribute="course" action="${adminPath}/course/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">课程名称</label>
   				<div class="col-sm-4">
					<form:input path="cname" htmlEscape="false" maxlength="64" class="form-control "/>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">任课老师</label>
   				<div class="col-sm-4">
					<form:select path="cteacher" class="form-control">
						<form:options items="${users}" itemLabel="trueName" itemValue="id"/>
					</form:select>
				</div>
			</div>
			
			<%-- <div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">课程时长</label>
   				<div class="col-sm-4">
					<form:input path="classhour" htmlEscape="false" maxlength="64" class="form-control "/>
				</div>
			</div> --%>
			
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">开始时间</label>
   				<div class="col-sm-7">
					<input name="stimeStr" htmlEscape="false" class="Wdate" readonly="readonly"
   					value="<fmt:formatDate value="${course.stime}" pattern="yyyy-MM-dd"/>"
   					 onFocus="WdatePicker({lang:'zh-cn',startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"/>
   				
				</div>
			</div>
			
			<div class="form-group">
				<label for="firstname" class="col-sm-1 control-label">结束时间</label>

   				<div class="col-sm-7">
					<input name="etimeStr" htmlEscape="false" class="Wdate" readonly="readonly"
   					value="<fmt:formatDate value="${course.etime}" pattern="yyyy-MM-dd"/>"
   					 onFocus="WdatePicker({lang:'zh-cn',startDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"/>
   				
				</div>
			</div>
			
			
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
	</form:form>
	</div>
	</div>
</body>
</html>