<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<title>互动详情</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<link href="${adminPath }/static/front/CSS/main.css" rel="stylesheet" type="text/css" />
	<script language="javascript">
	</script>
</head>
<body onload="focusOnLogin()">



<link href="CSS/read.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	/* function checkComment(){
		var content = document.getElementById("ccontent");
		if(content.value == ""){
			alert("评论内容不能为空！！");
			return false;
		}
		return true;
	} */
</script>

<div id="container">
  <div class="main">
    <div class="content1">
    
      <ul class="classlist">
        <table width="100%" align="center">
          <tr width="100%">
            <td colspan="2" align="center" style="font-size: 16px;">${active.title }</td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2" >
           	    课程名称：${active.couname}
          	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          	    发表教师：${active.username}
          	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          	    发布时间：<fmt:formatDate value="${active.stime}" pattern="yyyy-MM-dd HH:mm:ss"/>
          	     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
          	     <a href="${adminPath}/active/">返回</a>
          	    </td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
         
          <tr>
            <td colspan="2" align="center"></td>
          </tr>
          <tr>
            <td colspan="2">
            	${active.content}
            </td>
          </tr>
          <tr>
            <td colspan="2"><hr />
            </td>
          </tr>
        </table>
      </ul>
      
     <ul class="classlist">
        <table width="80%" align="center">
        <c:forEach items="${commentList}" var="comment">
        	<li><b style="font-size: 16px;color: orange;">${comment.stuname}：</b>
        	    ${comment.content }
        	    
        	    <c:if test="${curUser.roleId !=4 }">
        	    <a href="${adminPath}/reply/delete?id=${comment.id}" onclick="return confirm('确认要删除该评论吗？', this.href)">
        	    <button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							删除
						</button>
        	    </a>
        	    </c:if>
        	    </li>
        </c:forEach>
        </table>
      </ul> 
      <ul class="classlist">
        <form action="${adminPath }/reply/save?aid=${active.id}&tid=${active.undefine}&cid=${active.cid}&atime=<fmt:formatDate value="${active.stime}" pattern="yyyy/MM/dd HH:mm:ss"/>" method="post" onSubmit="return checkComment()">
          <table width="80%" align="center">
            <tr>
              <td> 评 论 </td>
            </tr>
            <tr>
              <td colspan="2"><textarea id="ccontent" name="content" cols="125" rows="5" class="xheditor"></textarea>
              </td>
            </tr>
            <td><input name="submit" value="发  表" type="submit"/>
              </td>
          </table>
        </form>
      </ul>
      
    </div>
  </div>
</div>
</body>
</html>