<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>成绩管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>
</head>
<body>
	<div class="panel panel-default">
	<div class="panel-heading">
	<ul class="nav nav-tabs">
		<li><a href="${adminPath}/result/">成绩列表</a></li>
	</ul>
	</div>
	<div class="panel-body">
	<form:form id="inputForm" modelAttribute="result" action="${adminPath}/result/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">学生姓名</label>
   				<div class="col-sm-4">
			
						<form:input path="stuname" htmlEscape="false" disable="true" maxlength="500" class="form-control  "/>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname"  class="col-sm-1 control-label">课程名称</label>
   				<div class="col-sm-4">
			
						<form:input path="couname" htmlEscape="false" maxlength="500"  disable="true" class="form-control  "/>
				</div>
			</div>
			
			<div class="form-group" id="roleSelect">
				<label for="firstname" class="col-sm-1 control-label">成绩</label>
   				<div class="col-sm-4">
					<form:input path="result" htmlEscape="false" placeholder="" maxlength="500" disable="true" class="form-control  "/>
				</div>
			</div>
			
			<button class="btn btn-success" type="submit">保 存</button>
			<button class="btn btn-inf" type="button" onclick="javascript:history.go(-1);">返 回</button>
	</form:form>
	</div>
	</div>
</body>
</html>