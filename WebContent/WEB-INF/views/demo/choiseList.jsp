<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>我的课程管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n){
			$("#pageNo").val(n);
			$("#searchForm").submit();
	       	return false;
	       }
	</script>
</head>
<body>
<div class="panel panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
		
			<c:if test="${user.roleId ==1 }">
				<li class="active"><a href="${adminPath}/choice/list">课程列表</a></li>
			</c:if>
			<c:if test="${user.roleId !=1 }">
			<li class="active"><a href="${adminPath}/choice/list">我的课程</a></li>
			</c:if>
			<c:if test="${user.roleId!=4 }">
			<li class="active"><a href="${adminPath}/choice/seleChoice/">学生选课列表</a></li>
			</c:if>
		</ul>
	</div>
	<c:if test="${!empty message }">
			<div id="myAlert" class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong></strong>${message}
			</div>
		</c:if>
	
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>序号</th>
				<th>课程</th>
				<th>任课老师</th>
				<!-- <th>选课学生</th> -->
				<!-- <th>操作</th> -->
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pageInfo.list}" var="choice"  varStatus="index_no">
			<tr>
				<td>${index_no.index+1+(pageInfo.pageNum-1)*pageInfo.pageSize }</td>
				<td>
					${choice.couname}
				</td>
					<td>
					${choice.teaname}
				</td>
					<%-- <td>
					${choice.stuname}
				</td> --%>
				<td>
				<c:if test="${user.roleId ==4 }">
					<a href="${adminPath}/choice/delete?id=${choice.id}" onclick="return confirm('确认要删除该课程吗？', this.href)">
						<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							删除
						</button>
					</a>
				</c:if>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<!-- 分页条 -->
		<div class="panel-footer">
			<!-- 显示分页信息 -->
		<div class="row">
			<!--分页文字信息  -->
			<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
				页,总 ${pageInfo.total } 条记录</div>
			<!-- 分页条信息 -->
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>


					<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
						<c:if test="${page_Num == pageInfo.pageNum }">
							<li class="active"><a href="#">${page_Num }</a></li>
						</c:if>
						<c:if test="${page_Num != pageInfo.pageNum }">
							<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
						</c:if>

					</c:forEach>
					<c:if test="${pageInfo.hasNextPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
</body>
</html>