<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>系统公告管理</title>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n){
			$("#pageNo").val(n);
			$("#searchForm").submit();
	       	return false;
	       }
	</script>
</head>
<body>
<div class="panel panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<c:if test="${user.roleId ==3 }">
			<li class="active"><a href="${adminPath}/notice/">教学内容列表</a></li>
			<li><a href="${adminPath}/notice/form">教学内容添加</a></li>
			</c:if>
			<c:if test="${user.roleId ==1 }">
			<li class="active"><a href="${adminPath}/notice/">系统公告列表</a></li>
			<li><a href="${adminPath}/notice/form">系统公告添加</a></li>
			</c:if>
			<c:if test="${user.roleId ==4 }">
			<li class="active"><a href="${adminPath}/notice/">内容/公告</a></li>
			</c:if>
		</ul>
	</div>
	<div class="panel-body">
	<form:form id="searchForm" modelAttribute="notice" action="${adminPath}/notice/" method="post" class="form-horizontal">
		<input id="pageNo" name="pageNo" type="hidden" value="${pageInfo.pageNum}"/>
		<div class="form-group">
		<label for="firstname" class="col-sm-1 control-label">标题：</label>
				<div class="col-sm-2">
					<form:input path="title" htmlEscape="false" maxlength="64" class="form-control "/>
   				</div>
			<label for="firstname" class="col-sm-1 control-label">公告类型：</label>
				<div class="col-sm-2">
					<form:select path="type" class="form-control">
						<form:option value="">--未选择--</form:option>
						<form:option value="1">教学内容</form:option>
						<form:option value="0">系统公告</form:option>
					</form:select>
   				</div>
			<button class="btn btn-success" type="submit">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				查询
			</button>
		</div>
	</form:form>
	</div>
	<c:if test="${!empty message }">
			<div id="myAlert" class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
				<strong></strong>${message}
			</div>
		</c:if>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>序号</th>
				<th>标题</th>
				<th>类型</th>
				<th>发布时间</th>
				<c:if test="${user.roleId !=4 }">
				<th>操作</th>
				</c:if>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pageInfo.list}" var="newsComment"  varStatus="index_no">
			<tr>
				<td>${index_no.index+1+(pageInfo.pageNum-1)*pageInfo.pageSize }</td>
				<td>
					${newsComment.title}
				</td>
				<td>
					${newsComment.type=="1"?"教学内容":"系统公告"}
				</td>
				<td>
					<fmt:formatDate value="${newsComment.addTime}" pattern="yyyy-MM-dd"/>
				</td>
				
				<c:if test="${user.roleId !=4 }">
				<td>
    				<a href="${adminPath}/notice/form?id=${newsComment.id}">
    					<button class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							编辑
						</button>
    				</a>
					<a href="${adminPath}/notice/delete?id=${newsComment.id}" onclick="return confirm('确认要删除该公告吗？', this.href)">
						<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							删除
						</button>
					</a>
				</td>
				</c:if>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<!-- 分页条 -->
		<div class="panel-footer">
			<!-- 显示分页信息 -->
		<div class="row">
			<!--分页文字信息  -->
			<div class="col-md-6">当前 ${pageInfo.pageNum }页,总${pageInfo.pages }
				页,总 ${pageInfo.total } 条记录</div>
			<!-- 分页条信息 -->
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});">首页</a></li>
					<c:if test="${pageInfo.hasPreviousPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum-1});"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
						</a></li>
					</c:if>


					<c:forEach items="${pageInfo.navigatepageNums }" var="page_Num">
						<c:if test="${page_Num == pageInfo.pageNum }">
							<li class="active"><a href="#">${page_Num }</a></li>
						</c:if>
						<c:if test="${page_Num != pageInfo.pageNum }">
							<li><a href="javascript:void(0);" onclick="page(${page_Num});">${page_Num }</a></li>
						</c:if>

					</c:forEach>
					<c:if test="${pageInfo.hasNextPage }">
						<li><a href="javascript:void(0);" onclick="page(${pageInfo.pageNum+1 });"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span>
						</a></li>
					</c:if>
					<li><a href="javascript:void(0);" onclick="page(${pageInfo.pages });">末页</a></li>
				</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
</body>
</html>